/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kazro
 */
public class CreatePDF {
    
    /**
     * creates the pay slip for the employee in PDF form
     *
     * @param filePath location to save the pay slip
     * @param employeeId employee id of the employee to create the pay slip for
     */
    public void generatePayslip(String filePath, String employeeId) {
        Employee pdfEmployee = new Employee();
        pdfEmployee.searchEmployee(employeeId);
        String nowStr = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        
        String title = pdfEmployee.getTitle();
        String forename = pdfEmployee.getForename();
        String surname = pdfEmployee.getSurname();
        String deptName = pdfEmployee.getDepartment();
        String taxCode = pdfEmployee.getTaxCode();
        String niCategory = pdfEmployee.getNationalCategory();

        String quantity = "";
        String rate = pdfEmployee.getPayRate();
        String pay = pdfEmployee.getLastGross();
        String tax = pdfEmployee.getLastIncomeTax();
        String niCont = pdfEmployee.getLastNationalContributions();

        try {
            Document myDocument = new Document();
            PdfWriter myWriter = PdfWriter.getInstance(myDocument, new FileOutputStream(filePath + "\\" + forename + " " + surname + " Payslip " + nowStr + ".pdf"));
            myDocument.open();

            myDocument.add(createEmployeeDetailsTable(employeeId, title, forename, surname, deptName, taxCode, niCategory));
            myDocument.add(createPeriodPayTable(quantity, rate, pay, tax, niCont));
            myDocument.add(createSummaryTable(employeeId));
            myDocument.add(new Paragraph(" "));
//            myDocument.add(createMessagesTable());

            myDocument.close();
        } catch (DocumentException | FileNotFoundException ex) {
            Logger.getLogger(CreatePDF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Creates the employee details table
     *
     * @return returns the employee details table populated with the employee's
     * details
     */
    private PdfPTable createEmployeeDetailsTable(String employeeId, String title, String forename, String surname, String departmentName, String taxCode, String niCategory) {

        PdfPTable periodPayTable = new PdfPTable(4);
        try {

            periodPayTable.setWidths(new int[]{1, 1, 1, 1});

            periodPayTable.addCell(createCell("EMPLOYEE ID", 5, 2, 2, 2, 1, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("EMPLOYEE NAME", 5, 2, 2, 0, 2, 0, Element.ALIGN_CENTER));

            periodPayTable.addCell(createCell(employeeId, 9, 2, 0, 2, 1, 1, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(title + " " + forename + " " + surname, 9, 2, 0, 0, 2, 1, Element.ALIGN_CENTER));

            periodPayTable.addCell(createCell("DEPARTMENT", 5, 2, 0, 2, 1, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("TAX CODE", 5, 1, 0, 0, 1, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("NI CATEGORY", 5, 1, 0, 0, 2, 0, Element.ALIGN_CENTER));

            periodPayTable.addCell(createCell(departmentName, 9, 2, 0, 2, 1, 1, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(taxCode, 9, 1, 0, 0, 1, 1, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(niCategory, 9, 1, 0, 0, 2, 1, Element.ALIGN_CENTER));
        } catch (DocumentException ex) {
            Logger.getLogger(CreatePDF.class.getName()).log(Level.SEVERE, null, ex);
        }
        return periodPayTable;
    }
    
    /**
     * Creates the period details table
     *
     * @return returns the period details table populated with the employee's
     * pay for this period
     */
    private PdfPTable createPeriodPayTable(String quantity, String rate, String pay, String deductions) {
        PdfPTable periodPayTable = new PdfPTable(6);
        int i;
        int additionalPaymentRows;
        int additionalDeductionRows;
        String payArrAll[] = pay.split("#");
        String deductArrAll[] = deductions.split("#");
        String payArr2d[][] = new String[payArrAll.length][4];
        String deductArr2d[][] = new String[deductArrAll.length][2];
        String joinedArr2d[][];
        // repeat for all payments
        for (i = 0; i < payArrAll.length; i++){
            payArr2d[i] = payArrAll[i].split("&");
        }
        // repeat for all deductions
        for (i = 0; i < deductArrAll.length; i++){
            deductArr2d[i] = deductArrAll[i].split("&");
        }
        
        
        additionalPaymentRows = 15 - payArrAll.length;
        additionalDeductionRows = 15 - deductArrAll.length;
        
        try {

            periodPayTable.setWidths(new int[]{3, 2, 2, 2, 3, 2});

            periodPayTable.addCell(createCell(" PAY AND ALLOWANCES", 5, 4, 1, 2, 1, 1, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" DEDUCTIONS", 5, 2, 1, 1, 2, 1, Element.ALIGN_LEFT));

            periodPayTable.addCell(createCell("DESCRIPTION", 5, 1, 0, 2, 1, 1, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("QUANTITY", 5, 1, 0, 0, 1, 1, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("RATE", 5, 1, 0, 0, 1, 1, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("AMOUNT", 5, 1, 0, 0, 1, 1, Element.ALIGN_CENTER));

            periodPayTable.addCell(createCell("DESCRIPTION", 5, 1, 0, 1, 1, 1, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("AMOUNT", 5, 1, 0, 0, 2, 1, Element.ALIGN_CENTER));

            for(i = 0; i < deductArr2d.length; i++){
                periodPayTable.addCell(createCell(" " + deductArr2d[i][0], 7, 1, 0, 2, 1, 0, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" " + deductArr2d[i][1], 7, 1, 0, 0, 1, 0, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" " + deductArr2d[i][2], 7, 1, 0, 0, 1, 0, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" " + deductArr2d[i][2], 7, 1, 0, 0, 1, 0, Element.ALIGN_LEFT));
            }
            
            periodPayTable.addCell(createCell("TAX", 7, 1, 0, 1, 1, 0, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" " + tax, 7, 1, 0, 0, 2, 0, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" ", 7, 1, 0, 2, 1, 0, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" ", 7, 1, 0, 0, 1, 0, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" ", 7, 1, 0, 0, 1, 0, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" ", 7, 1, 0, 0, 1, 0, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell("NI CONTS", 7, 1, 0, 1, 1, 0, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" " + niCont, 7, 1, 0, 0, 2, 0, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" ", 7, 1, 0, 2, 1, 1, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" ", 7, 1, 0, 0, 1, 1, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" ", 7, 1, 0, 0, 1, 1, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" ", 7, 1, 0, 0, 1, 1, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" ", 7, 1, 0, 1, 1, 1, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" ", 7, 1, 0, 0, 2, 1, Element.ALIGN_LEFT));
        } catch (DocumentException ex) {
            Logger.getLogger(CreatePDF.class.getName()).log(Level.SEVERE, null, ex);
        }
        return periodPayTable;
    }
    
    /**
     * Creates the summary details table
     *
     * @return returns the summary details table populated with the employee's
     * pay summary
     */
    private PdfPTable createSummaryTable(String employeeId) {
        Employee pdfYearEmployee = new Employee();
        pdfYearEmployee.searchEmployee(employeeId);

        PdfPTable periodPayTable = new PdfPTable(6);
        
        String periodEnd = getPeriodEnd();
        String now = getDateNow();
        String taxPeriod = getTaxPeriod();
        
        try {

            periodPayTable.setWidths(new int[]{1, 1, 1, 1, 1, 1});

            periodPayTable.addCell(createCell(" YEAR TO DATE BALANCES", 5, 2, 1, 2, 1, 1, Element.ALIGN_LEFT));
            periodPayTable.addCell(createCell(" THIS PERIOD SUMMARY", 5, 4, 1, 1, 2, 1, Element.ALIGN_LEFT));

            periodPayTable.addCell(createCell("GROSS PAY", 5, 1, 0, 2, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" " + pdfYearEmployee.getYearGross(), 5, 1, 0, 0, 1, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("TAX PERIOD", 5, 1, 0, 1, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(taxPeriod, 5, 1, 0, 0, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("TAXABLE PAY", 5, 1, 0, 1, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" " + pdfYearEmployee.getLastTaxable(), 5, 1, 0, 0, 2, 0, Element.ALIGN_CENTER));

            periodPayTable.addCell(createCell("TAXABLE PAY", 5, 1, 0, 2, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" " + pdfYearEmployee.getYearTaxable(), 5, 1, 0, 0, 1, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("FREQUENCY", 5, 1, 0, 1, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("MONTHLY", 5, 1, 0, 0, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("NON-TAXABLE PAY", 5, 1, 0, 1, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" ", 5, 1, 0, 0, 2, 0, Element.ALIGN_CENTER));

            periodPayTable.addCell(createCell("TAX", 5, 1, 0, 2, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" " + pdfYearEmployee.getYearIncomeTax(), 5, 1, 0, 0, 1, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("PERIOD END", 5, 1, 0, 1, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(periodEnd, 5, 1, 0, 0, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("GROSS PAY", 5, 1, 0, 1, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" " + pdfYearEmployee.getLastGross(), 5, 1, 0, 0, 2, 0, Element.ALIGN_CENTER));

            periodPayTable.addCell(createCell("NI CONTS", 5, 1, 0, 2, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" " + pdfYearEmployee.getYearNationalContributions(), 5, 1, 0, 0, 1, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("PAY DATE", 5, 1, 0, 1, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(now, 5, 1, 0, 0, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("TOTAL DEDUCTIONS", 5, 1, 0, 1, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("VALUE", 5, 1, 0, 0, 2, 0, Element.ALIGN_CENTER));

            periodPayTable.addCell(createCell("EMPLOYER NI", 5, 1, 0, 2, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" " + pdfYearEmployee.getYearEmployerContributions(), 5, 1, 0, 0, 1, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("NI CONTS", 5, 1, 0, 1, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" " + pdfYearEmployee.getLastNationalContributions(), 5, 1, 0, 0, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("NET PAY", 5, 1, 2, 2, 0, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" " + pdfYearEmployee.getLastNet(), 9, 1, 2, 0, 2, 0, Element.ALIGN_CENTER));

            periodPayTable.addCell(createCell("NET PAY", 5, 1, 0, 2, 0, 2, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" " + pdfYearEmployee.getYearNet(), 5, 1, 0, 0, 1, 2, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell("EMPLOYER CONTS", 5, 1, 0, 1, 0, 2, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" " + pdfYearEmployee.getLastEmployerContributions(), 5, 1, 0, 0, 0, 2, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" ", 5, 1, 0, 2, 0, 2, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" ", 7, 1, 0, 0, 2, 2, Element.ALIGN_CENTER));
        } catch (DocumentException ex) {
            Logger.getLogger(CreatePDF.class.getName()).log(Level.SEVERE, null, ex);
        }
        return periodPayTable;
    }
    
    /**
     * Creates the message details table
     *
     * @return returns the message details table with any messages from the
     * employer
     */
    private PdfPTable createMessagesTable() {

        PdfPTable periodPayTable = new PdfPTable(2);
        try {

            periodPayTable.setWidths(new int[]{1, 1});

            periodPayTable.addCell(createCell("MESSAGES FROM EMPLOYER", 9, 2, 2, 2, 2, 1, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" ", 9, 2, 0, 2, 2, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" ", 9, 2, 0, 2, 2, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" ", 9, 2, 0, 2, 2, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" ", 9, 2, 0, 2, 2, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" ", 9, 2, 0, 2, 2, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" ", 9, 2, 0, 2, 2, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" ", 9, 2, 0, 2, 2, 0, Element.ALIGN_CENTER));
            periodPayTable.addCell(createCell(" ", 9, 2, 0, 2, 2, 2, Element.ALIGN_CENTER));

        } catch (DocumentException ex) {
            Logger.getLogger(CreatePDF.class.getName()).log(Level.SEVERE, null, ex);
        }
        return periodPayTable;
    }
    
    /**
     * gets the last day of the month
     * @return returns the date of the last day of the current month in the format DD/MM/YYYY
     */
    private String getPeriodEnd(){
        LocalDate lastDate = LocalDate.now().with(lastDayOfMonth());
        String lastDateStr = lastDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return lastDateStr;
    }
   
    /**
     * gets the date of the device
     * @return returns the current date in the format DD/MM/YYYY
     */
    private String getDateNow(){
        LocalDate now = LocalDate.now();
        String nowStr = now.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return nowStr;
    }
        
    private String getTaxPeriod(){
        LocalDate now = LocalDate.now();
        int taxMonth = 0;
        LocalDate taxMonth1Start = LocalDate.of(now.getYear(), Month.APRIL, 6);
        LocalDate taxMonth1End = LocalDate.of(now.getYear(), Month.MAY, 5);
        LocalDate taxMonth2Start = LocalDate.of(now.getYear(), Month.MAY, 6);
        LocalDate taxMonth2End = LocalDate.of(now.getYear(), Month.JUNE, 5);
        LocalDate taxMonth3Start = LocalDate.of(now.getYear(), Month.JUNE, 6);
        LocalDate taxMonth3End = LocalDate.of(now.getYear(), Month.JULY, 5);
        LocalDate taxMonth4Start = LocalDate.of(now.getYear(), Month.JULY, 6);
        LocalDate taxMonth4End = LocalDate.of(now.getYear(), Month.AUGUST, 5);
        LocalDate taxMonth5Start = LocalDate.of(now.getYear(), Month.AUGUST, 6);
        LocalDate taxMonth5End = LocalDate.of(now.getYear(), Month.SEPTEMBER, 5);
        LocalDate taxMonth6Start = LocalDate.of(now.getYear(), Month.SEPTEMBER, 6);
        LocalDate taxMonth6End = LocalDate.of(now.getYear(), Month.OCTOBER, 5);
        LocalDate taxMonth7Start = LocalDate.of(now.getYear(), Month.OCTOBER, 6);
        LocalDate taxMonth7End = LocalDate.of(now.getYear(), Month.NOVEMBER, 5);
        LocalDate taxMonth8Start = LocalDate.of(now.getYear(), Month.NOVEMBER, 6);
        LocalDate taxMonth8End = LocalDate.of(now.getYear(), Month.DECEMBER, 5);
        LocalDate taxMonth9Start = LocalDate.of(now.getYear(), Month.DECEMBER, 6);
        LocalDate taxMonth9End = LocalDate.of(now.getYear(), Month.JANUARY, 5);
        LocalDate taxMonth10Start = LocalDate.of(now.getYear(), Month.JANUARY, 6);
        LocalDate taxMonth10End = LocalDate.of(now.getYear(), Month.FEBRUARY, 5);
        LocalDate taxMonth11Start = LocalDate.of(now.getYear(), Month.FEBRUARY, 6);
        LocalDate taxMonth11End = LocalDate.of(now.getYear(), Month.MARCH, 5);
        LocalDate taxMonth12Start = LocalDate.of(now.getYear(), Month.MARCH, 6);
        LocalDate taxMonth12End = LocalDate.of(now.getYear(), Month.APRIL, 5);
        if(now.isBefore(taxMonth1Start) || now.isAfter(taxMonth1End) == false){
            taxMonth = 1;
        }
        else if(now.isBefore(taxMonth2Start) || now.isAfter(taxMonth2End) == false){
            taxMonth = 2;
        }
        else if(now.isBefore(taxMonth3Start) || now.isAfter(taxMonth3End) == false){
            taxMonth = 3;
        }
        else if(now.isBefore(taxMonth4Start) || now.isAfter(taxMonth4End) == false){
            taxMonth = 4;
        }
        else if(now.isBefore(taxMonth5Start) || now.isAfter(taxMonth5End) == false){
            taxMonth = 5;
        }
        else if(now.isBefore(taxMonth6Start) || now.isAfter(taxMonth6End) == false){
            taxMonth = 6;
        }
        else if(now.isBefore(taxMonth7Start) || now.isAfter(taxMonth7End) == false){
            taxMonth = 7;
        }
        else if(now.isBefore(taxMonth8Start) || now.isAfter(taxMonth8End) == false){
            taxMonth = 8;
        }
        else if(now.isBefore(taxMonth9Start) || now.isAfter(taxMonth9End) == false){
            taxMonth = 9;
        }
        else if(now.isBefore(taxMonth10Start) || now.isAfter(taxMonth10End) == false){
            taxMonth = 10;
        }
        else if(now.isBefore(taxMonth11Start) || now.isAfter(taxMonth11End) == false){
            taxMonth = 11;
        }
        else if(now.isBefore(taxMonth12Start) || now.isAfter(taxMonth12End) == false){
            taxMonth = 12;
        }
        return String.valueOf(taxMonth);
    }
    
    /**
     * creates a cell with the specified design and content
     *
     * @param content text to display within the cell
     * @param fontSize size of the font to use
     * @param colspan columns this cell occupies in the table
     * @param topWidth width of the top border
     * @param leftWidth width of the left border
     * @param rightWidth width of the right border
     * @param bottomWidth width of the bottom border
     * @param alignment alignment of the text
     * @return returns the created cell
     */
    private PdfPCell createCell(String content, int fontSize, int colspan, int topWidth, int leftWidth, int rightWidth, int bottomWidth, int alignment) {

        com.itextpdf.text.Font font = FontFactory.getFont("Times Roman", fontSize);
        PdfPCell cell = new PdfPCell(new Phrase(content, font));

        cell.setBorderWidthTop(topWidth);
        cell.setBorderWidthLeft(leftWidth);
        cell.setBorderWidthRight(rightWidth);
        cell.setBorderWidthBottom(bottomWidth);
        cell.setColspan(colspan);
        cell.setHorizontalAlignment(alignment);

        return cell;

    }
}
