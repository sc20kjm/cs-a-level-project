/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author MAD16833376
 */
public class EmployeeTableModel extends DefaultTableModel {

    final static String employeeFileDir = "Employee Files\\"; // employee files location

    final static String personalDetailsFileName = "Personal Details";

    final static File personalDetailsFile = new File(employeeFileDir + personalDetailsFileName + ".csv");

    public static void clear(JTable table) {
        // clears contents of the table
        int rowCount = table.getRowCount();
        int columnCount = table.getColumnCount();
        int i;
        int j;

        for (i = 0; i < rowCount; i++) {
            for (j = 0; j < columnCount; j++) {
                table.setValueAt("", i, j);
            }
        }
    }
    
    public static void empty(JTable table){
        DefaultTableModel tableModel;
        tableModel = new DefaultTableModel();
        table.setModel(tableModel);
        tableModel.addColumn("Employee ID");
        tableModel.addColumn("Surname");
        tableModel.addColumn("Forename");
        tableModel.addColumn("Department");
        tableModel.addColumn("NI Category");
        tableModel.addColumn("TaxCode");
        tableModel.addColumn("Pay Rate");
        tableModel.addColumn("Last Net Pay");
        tableModel.addColumn("Last Calculated");
    }

    public static void initialise(JTable table) {
        DefaultTableModel tableModel;
        tableModel = new DefaultTableModel();

        Employee employeeDetails = new Employee();

        String employeeID;
        String line;    // stores current line in file

        table.setModel(tableModel);
        tableModel.addColumn("Employee ID");
        tableModel.addColumn("Surname");
        tableModel.addColumn("Forename");
        tableModel.addColumn("Department");
        tableModel.addColumn("NI Category");
        tableModel.addColumn("TaxCode");
        tableModel.addColumn("Pay Rate");
        tableModel.addColumn("Last Net Pay");
        tableModel.addColumn("Last Calculated");

        try {
            // create reader objects for personal details file
            FileReader personalReader;
            personalReader = new FileReader(personalDetailsFile);
            BufferedReader personalBuff = new BufferedReader(personalReader);
            personalBuff.readLine();    // skip headers

            while ((line = personalBuff.readLine()) != null) {    // repeats until end of file
                employeeDetails.setPersonalRecord(line);    // store personal record
                employeeID = line.substring(0, 8);  // obtain employee ID to search employment record

                employeeDetails.searchEmployment(employeeID);
                // display employee details to the table
                tableModel.addRow(new Object[]{employeeDetails.getEmployeeID(), employeeDetails.getSurname(), employeeDetails.getForename(),
                    employeeDetails.getDepartment(), employeeDetails.getNationalCategory(), employeeDetails.getTaxCode(), employeeDetails.getPayRate(), employeeDetails.getLastNet(), employeeDetails.getLastCalculated()});
            }
            personalReader.close();
        } catch (IOException ex) {
            System.out.println("Error reading " + personalDetailsFile);
        }
    }

    public static void displaySearch(JTable table, ArrayList<String> results) {
        DefaultTableModel tableModel;
        tableModel = new DefaultTableModel();

        Employee employeeDetails = new Employee();
        int i;

        table.setModel(tableModel);
        tableModel.addColumn("Employee ID");
        tableModel.addColumn("Surname");
        tableModel.addColumn("Forename");
        tableModel.addColumn("Department");
        tableModel.addColumn("NI Category");
        tableModel.addColumn("TaxCode");
        tableModel.addColumn("Pay Rate");
        tableModel.addColumn("Last Net Pay");
        tableModel.addColumn("Last Calculated");
        
        for (i = 0; i < results.size(); i++) {
            employeeDetails.searchEmployee(results.get(i));
            // display employee details to the table
            tableModel.addRow(new Object[]{employeeDetails.getEmployeeID(), employeeDetails.getSurname(), employeeDetails.getForename(),
                employeeDetails.getDepartment(), employeeDetails.getNationalCategory(), employeeDetails.getTaxCode(), employeeDetails.getPayRate(), employeeDetails.getLastNet(), employeeDetails.getLastCalculated()});

        }

    }
}
