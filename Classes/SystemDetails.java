/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kazro
 */
public class SystemDetails{
    
    String systemFileDir = "System Files\\";   // system files location
    
    String taxValuesFileName = "Tax Values";
    String nationalInsuranceValuesFileName = "National Insurance Values";
    
    File taxValuesFile = new File(systemFileDir + taxValuesFileName + ".csv");
    File nationalInsuranceValuesFile = new File(systemFileDir + nationalInsuranceValuesFileName + ".csv");
    
    private String backupFrequency;
    
    // standard values for tax calculation
    BigDecimal standardPAYearly;
    BigDecimal basicTaxLowerBound;
    BigDecimal basicTaxUpperBound;
    BigDecimal basicTaxRate;
    BigDecimal higherTaxLowerBound;
    BigDecimal higherTaxUpperBound;
    BigDecimal higherTaxRate;
    BigDecimal additionalTaxLowerBound;
    BigDecimal additionalTaxRate;
    
    // standard values for national insurance calculation
    BigDecimal primaryThreshold;
    BigDecimal upperEarningsLimit;
    BigDecimal aPrimaryRate;
    BigDecimal aUpperRate;
    BigDecimal bPrimaryRate;
    BigDecimal bUpperRate;
    BigDecimal cPrimaryRate;
    BigDecimal cUpperRate;
    BigDecimal hPrimaryRate;
    BigDecimal hUpperRate;
    BigDecimal jPrimaryRate;
    BigDecimal jUpperRate;
    BigDecimal mPrimaryRate;
    BigDecimal mUpperRate;
    BigDecimal zPrimaryRate;
    BigDecimal zUpperRate;
    BigDecimal aSecondaryRate;
    BigDecimal aUpperSecondaryRate;
    BigDecimal bSecondaryRate;
    BigDecimal bUpperSecondaryRate;
    BigDecimal cSecondaryRate;
    BigDecimal cUpperSecondaryRate;
    BigDecimal hSecondaryRate;
    BigDecimal hUpperSecondaryRate;
    BigDecimal jSecondaryRate;
    BigDecimal jUpperSecondaryRate;
    BigDecimal mSecondaryRate;
    BigDecimal mUpperSecondaryRate;
    BigDecimal zSecondaryRate;
    BigDecimal zUpperSecondaryRate;
    
    /**
     * set the standard values for calculating payroll from the respective files
     */
    public void setStandardPayrollValues(){
        setTaxValues();
        setNationalInsuranceValues();
    }
    
    /**
     * Obtain default values for tax calculation from tax values file
     */
    private void setTaxValues() {

        String line;
        String splitBy = ",";
        String[] taxRecord; // stores record for tax values

        try {
            // create reader objects
            FileReader taxRead = new FileReader(taxValuesFile);
            BufferedReader taxBuff = new BufferedReader(taxRead);
            
            
            while ((line = taxBuff.readLine()) != null) {
                taxRecord = line.split(splitBy);
                switch (taxRecord[0]) {
                    case "Standard Personal Allowance":
                        standardPAYearly = new BigDecimal(taxRecord[1]);
                        break;
                    case "Basic Tax Lower Bound":
                        basicTaxLowerBound = new BigDecimal(taxRecord[1]);
                        break;
                    case "Basic Tax Upper Bound":
                        basicTaxUpperBound = new BigDecimal(taxRecord[1]);
                        break;
                    case "Basic Tax Rate":
                        basicTaxRate = new BigDecimal(taxRecord[1]);
                        break;
                    case "Higher Tax Lower Bound":
                        higherTaxLowerBound = new BigDecimal(taxRecord[1]);
                        break;
                    case "Higher Tax Upper Bound":
                        higherTaxUpperBound = new BigDecimal(taxRecord[1]);
                        break;
                    case "Higher Tax Rate":
                        higherTaxRate = new BigDecimal(taxRecord[1]);
                        break;
                    case "Additional Tax Upper Bound":
                        additionalTaxLowerBound = new BigDecimal(taxRecord[1]);
                        break;
                    case "Additional Tax Rate":
                        additionalTaxRate = new BigDecimal(taxRecord[1]);
                        break;
                }
            }            
            // close readers
            taxRead.close();
            taxBuff.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SystemDetails.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SystemDetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Obtain default values for national insurance calculation from national insurance values file
     */
    private void setNationalInsuranceValues() {

        String line;
        String splitBy = ",";
        String[] nationalRecord;    // stores record for national insurance values

        try {
            
            FileReader nationalRead = new FileReader(nationalInsuranceValuesFile);
            BufferedReader nationalBuff = new BufferedReader(nationalRead);
            
            
            while ((line = nationalBuff.readLine()) != null) {
                nationalRecord = line.split(splitBy);
                switch (nationalRecord[0]) {
                    case "Primary Threshold":
                        primaryThreshold = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Upper Earnings Limit":
                        upperEarningsLimit = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category A Primary Rate":
                        aPrimaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category A Upper Rate":
                        aUpperRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category B Primary Rate":
                        bPrimaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category B Upper Rate":
                        bUpperRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category C Primary Rate":
                        cPrimaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category C Upper Rate":
                        cUpperRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category H Primary Rate":
                        hPrimaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category H Upper Rate":
                        hUpperRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category J Primary Rate":
                        jPrimaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category J Upper Rate":
                        jUpperRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category M Primary Rate":
                        mPrimaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category M Upper Rate":
                        mUpperRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category Z Primary Rate":
                        zPrimaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category Z Upper Rate":
                        zUpperRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category A Secondary Rate":
                        aSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category A Upper Secondary Rate":
                        aUpperSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category B Secondary Rate":
                        bSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category B Upper Secondary Rate":
                        bUpperSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category C Secondary Rate":
                        cSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category C Upper Secondary Rate":
                        cUpperSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category H Secondary Rate":
                        hSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category H Upper Secondary Rate":
                        hUpperSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category J Secondary Rate":
                        jSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category J Upper Secondary Rate":
                        jUpperSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category M Secondary Rate":
                        mSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category M Upper Secondary Rate":
                        mUpperSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category Z Secondary Rate":
                        zSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                    case "Category Z Upper Secondary Rate":
                        zUpperSecondaryRate = new BigDecimal(nationalRecord[1]);
                        break;
                }
            }
            // close readers
            nationalRead.close();
            nationalBuff.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SystemDetails.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SystemDetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setBasicTaxRate(BigDecimal basicTaxRate){
        this.basicTaxRate = basicTaxRate;
    }
    
    public void setHigherTaxRate(BigDecimal higherTaxRate){
        this.higherTaxRate = higherTaxRate;
    }
    
    public void setAdditionalTaxRate(BigDecimal additionalTaxRate){
        this.additionalTaxRate = additionalTaxRate;
    }
    
    public String getBackupFrequency(){
        return this.backupFrequency;
    }
    
    public BigDecimal getStandardPersonalAllowance(){
        return this.standardPAYearly;
    }
    
    public BigDecimal getBasicTaxLowerBound(){
        return this.basicTaxLowerBound;
    }
    public BigDecimal getBasicTaxUpperBound(){
        return this.basicTaxUpperBound;
    }
    public BigDecimal getBasicTaxRate(){
        return this.basicTaxRate;
    }
    
    public BigDecimal getHigherTaxLowerBound(){
        return this.higherTaxLowerBound;
    }
    public BigDecimal getHigherTaxUpperBound(){
        return this.higherTaxUpperBound;
    }
    public BigDecimal getHigherTaxRate(){
        return this.higherTaxRate;
    }
    
    public BigDecimal getAdditionalTaxLowerBound(){
        return this.additionalTaxLowerBound;
    }
    public BigDecimal getAdditionalTaxRate(){
        return this.additionalTaxRate;
    }
    
    
    public BigDecimal getPrimaryThreshold(){
        return this.primaryThreshold;
    }    
    public BigDecimal getUpperEarningsLimit(){
        return this.upperEarningsLimit;
    }
    
    public BigDecimal getCategoryAPrimaryRate(){
        return this.aPrimaryRate;
    }
    public BigDecimal getCategoryAUpperRate(){
        return this.aUpperRate;
    }
    
    public BigDecimal getCategoryBPrimaryRate(){
        return this.bPrimaryRate;
    }
    public BigDecimal getCategoryBUpperRate(){
        return this.bUpperRate;
    }
    
    public BigDecimal getCategoryCPrimaryRate(){
        return this.cPrimaryRate;
    }
    public BigDecimal getCategoryCUpperRate(){
        return this.cUpperRate;
    }
    
    public BigDecimal getCategoryHPrimaryRate(){
        return this.hPrimaryRate;
    }
    public BigDecimal getCategoryHUpperRate(){
        return this.hUpperRate;
    }
    
    public BigDecimal getCategoryJPrimaryRate(){
        return this.jPrimaryRate;
    }
    public BigDecimal getCategoryJUpperRate(){
        return this.jUpperRate;
    }
    
    public BigDecimal getCategoryMPrimaryRate(){
        return this.mPrimaryRate;
    }
    public BigDecimal getCategoryMUpperRate(){
        return this.mUpperRate;
    }
    
    public BigDecimal getCategoryZPrimaryRate(){
        return this.zPrimaryRate;
    }
    public BigDecimal getCategoryZUpperRate(){
        return this.zUpperRate;
    }
    
    
    public BigDecimal getCategoryASecondaryRate(){
        return this.aSecondaryRate;
    }
    public BigDecimal getCategoryAUpperSecondaryRate(){
        return this.aUpperSecondaryRate;
    }
    
    public BigDecimal getCategoryBSecondaryRate(){
        return this.bSecondaryRate;
    }
    public BigDecimal getCategoryBUpperSecondaryRate(){
        return this.bUpperSecondaryRate;
    }
    
    public BigDecimal getCategoryCSecondaryRate(){
        return this.cSecondaryRate;
    }
    public BigDecimal getCategoryCUpperSecondaryRate(){
        return this.cUpperSecondaryRate;
    }
    
    public BigDecimal getCategoryHSecondaryRate(){
        return this.hSecondaryRate;
    }
    public BigDecimal getCategoryHUpperSecondaryRate(){
        return this.hUpperSecondaryRate;
    }
    
    public BigDecimal getCategoryJSecondaryRate(){
        return this.jSecondaryRate;
    }
    public BigDecimal getCategoryJUpperSecondaryRate(){
        return this.jUpperSecondaryRate;
    }
    
    public BigDecimal getCategoryMSecondaryRate(){
        return this.mSecondaryRate;
    }
    public BigDecimal getCategoryMUpperSecondaryRate(){
        return this.mUpperSecondaryRate;
    }
    
    public BigDecimal getCategoryZSecondaryRate(){
        return this.zSecondaryRate;
    }
    public BigDecimal getCategoryZUpperSecondaryRate(){
        return this.zUpperSecondaryRate;
    }    
    
}
