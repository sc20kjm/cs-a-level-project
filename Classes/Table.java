/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JTable;

/**
 *
 * @author MAD16833376
 */
public class Table {
    /**
     * displays all data from chosen file into the chosen table
     * @param fileName  name of the file to read from
     * @param tableName name of the table to display data
     */
    public void display(String fileName, javax.swing.JTable tableName){
        String line = "";
        String splitBy = ",";
        String[] savedData;
        int i;
        int tableRow = 0;
        try{
            
            FileReader employeeReader = new FileReader(fileName);
            BufferedReader employeeBuff = new BufferedReader(employeeReader);
            
            while((line = employeeBuff.readLine()) != null){
                savedData = line.split(splitBy);
                for(i = 0; i < savedData.length; i ++){
                    tableName.setValueAt(savedData[i], tableRow, i++);
                }
            }
            tableRow ++;
            employeeReader.close();
        }
        catch (IOException ex){
            System.out.println("Error reading " + fileName);
        }
    }
    public static void display(String[][] array, JTable table){
        int i;
        int j;
        for (i = 0; i < array.length; i ++){
            for(j = 0; j < array[0].length; j ++){
                table.setValueAt(array[i][j], i, j);
            }
        }
    }
}
