package Classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author MAD16833376
 */
public class Validation {
    // format
    // type
    // length
    // presence
    // range

    /**
     * Presence check
     * @param input
     * @return 
     */
    public static boolean present(String input) {
        boolean valid = true;
        if (input.equals("")) {
            valid = false;
        }
        return valid;
    }
    
    /**
     * Type check for integer type
     * @param input - string to validate
     * @return      - true if input is an integer
     */
    public static boolean isInt(String input) {
        boolean valid = true;
        try {
            Integer.parseInt(input);
            valid = true;
        } catch (NumberFormatException e) {
            valid = false;
        }
        return valid;
    }

    /**
     * Length check string must be specified length
     *
     * @param input - input to validate
     * @param length - input must be this length
     * @return
     */
    public static boolean length(String input, int length) {
        boolean valid = true;
        if (input.replaceAll("\\s", "").length() != length) {
            valid = false;
        }
        return valid;
    }

    /**
     * Range check
     * @param input
     * @param min - least acceptable value of input
     * @param max - greatest acceptable value of input
     * @return 
     */
    public static boolean range(int input, int min, int max) {
        boolean valid = true;
        if (input < min || input > max) {
            valid = false;
        }
        return valid;
    }

    /**
     * Format check for date in the format (DD/MM/YYYY)
     * @param input
     * @return - true if input is in the correct format
     */
    public static boolean isDate(String input) {
//        System.out.println("ran");
        boolean valid = true;
        SimpleDateFormat dateFormat;
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        try {
            dateFormat.setLenient(false);
            dateFormat.parse(input);
            valid = true;

        } catch (ParseException p) {
            valid = false;
        }
//        System.out.println(valid);
        return valid;
    }

    public static boolean formatID(String inputStr) {
        boolean valid = true;
        valid = inputStr.matches("EMP-" + "[A-Z]{3}" + "[0-9]{5}");
        return valid;
    }
}
