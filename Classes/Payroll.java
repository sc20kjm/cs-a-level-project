/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 *
 * @author MAD16833376
 */
public class Payroll{
    
    public static ListIterator<String> employeeIterator;

    final String employeeFileDir = "Employee Files\\"; // employee files location

    final String transactionFileName = "Transaction File"; // name of transaction file

    // construct files
    File transactionFile = new File(employeeFileDir + transactionFileName + ".csv");
    
    String basicQuantityStr;
    String basicAmountStr;

    // Global variables used as they are used in multiple subroutines
    BigDecimal grossYearly;
    BigDecimal taxableYearly;
    BigDecimal taxYearly;
    BigDecimal basicTaxYearly;
    BigDecimal higherTaxYearly;
    BigDecimal additionalTaxYearly;
    BigDecimal nationalContributionsYearly;
    BigDecimal employerContributionsYearly;
    BigDecimal netYearly;

    BigDecimal additionalPayPreTax = BigDecimal.ZERO;
    BigDecimal additionalPayPostTax = BigDecimal.ZERO;
    BigDecimal additionalDeductionsPreTax = BigDecimal.ZERO;
    BigDecimal additionalDeductionsPostTax = BigDecimal.ZERO;
    BigDecimal grossPeriod;
    BigDecimal taxablePeriod;
    BigDecimal taxPeriod = BigDecimal.ZERO;
    BigDecimal nationalContributionsPeriod = BigDecimal.ZERO;
    BigDecimal employerContributionsPeriod = BigDecimal.ZERO;
    BigDecimal netPeriod;
    
    BigDecimal grossYearUpdated;
    BigDecimal taxableYearUpdated;
    BigDecimal taxYearUpdated;
    BigDecimal contributionsYearUpdated;
    BigDecimal employerContributionsYearUpdated;
    BigDecimal netYearUpdated;

    BigDecimal yearFreq = BigDecimal.ONE;

    BigDecimal primaryContributions;
    BigDecimal upperContributions;

    BigDecimal personalAllowanceYearly;

    BigDecimal partnersIncomeYearly;
    BigDecimal partnersPAYearly;

    

    

    boolean calculated = false; // indicates whether calculation has been made
    
    Employee employee = new Employee();
    SystemDetails payroll = new SystemDetails();
    
    /**
     * Calculates the any additional pay before tax
     * @param amount - amount of pay to add before tax
     */
    public void addAdditionalPayPreTax(BigDecimal amount){
        additionalPayPreTax = additionalPayPreTax.add(amount);
    }
    
    /**
     * Calculates the any additional pay after tax
     * @param amount - amount of pay to add after tax
     */
    public void addAdditionalPayPostTax(BigDecimal amount){
        additionalPayPostTax = additionalPayPostTax.add(amount);
    }
    
    /**
     * Calculates the any additional pay before tax
     * @param amount amount of pay to deduct before tax
     */
    public void addAdditionalDeductionsPreTax(BigDecimal amount){
        additionalDeductionsPreTax = additionalDeductionsPreTax.add(amount);
    }
    
    /**
     * Calculates the any additional pay after tax
     * @param amount amount of pay to deduct before tax
     */
    public void addAdditionalDeductionsPostTax(BigDecimal amount){
        additionalDeductionsPostTax = additionalDeductionsPostTax.add(amount);
    }
    
    /**
     * Calculates the employee's gross pay for the week
     * @param payRate - the amount this employee is paid per hour
     * @param payQuantity - the amount of hours this employee has worked
     */
    private void grossPayYearly(BigDecimal payRate, BigDecimal payQuantity) {
        BigDecimal basicAmount = payRate.multiply(payQuantity);
        BigDecimal totalAdditionalPreTax = additionalPayPreTax.subtract(additionalDeductionsPreTax);
        System.out.println("totalAdditionalPreTax= " + totalAdditionalPreTax);
        
        grossPeriod = basicAmount.add(totalAdditionalPreTax);
        BigDecimal weekly = new BigDecimal("52");
        grossYearly = grossPeriod.multiply(weekly);
    }
    
    /**
     * Calculates employee's personal allowance for the year
     */
    private void personalAllowanceYear() {
        BigDecimal aboveLimit;
        BigDecimal maxNoReduction = new BigDecimal("100000");
        BigDecimal two = new BigDecimal("2");
        BigDecimal personalAllowanceReduction;
        if (grossYearly.compareTo(maxNoReduction) > 0) {
            aboveLimit = grossYearly.subtract(maxNoReduction);   //grossYearly - 100000;
            personalAllowanceReduction = aboveLimit.divide(two, RoundingMode.FLOOR);
            personalAllowanceYearly = payroll.getStandardPersonalAllowance().subtract(personalAllowanceReduction);  //personalAllowanceYearly - personalAllowanceReduction;
            if (personalAllowanceYearly.compareTo(BigDecimal.ZERO) < 0) {
                personalAllowanceYearly = BigDecimal.ZERO;
            }
        } else {
            personalAllowanceYearly = payroll.getStandardPersonalAllowance();
        }
    }
    
    /**
     * Calculates amount of income that pays tax
     */
    private void taxableYearly() {
        BigDecimal period = new BigDecimal("52");
        if (grossYearly.compareTo(personalAllowanceYearly) > 0) {
            taxableYearly = grossYearly.subtract(personalAllowanceYearly);
            taxablePeriod = taxableYearly.divide(period, 2, RoundingMode.HALF_EVEN);
        } else {
            taxableYearly = BigDecimal.ZERO;
            taxablePeriod = BigDecimal.ZERO;
        }
    }
    
    /**
     * Calculates the amount of tax the employee pays
     */
    private void incomeTaxYearly() {
        BigDecimal period = new BigDecimal("52");
        // greater than higher tax bracket 
        if (grossYearly.compareTo(payroll.getHigherTaxUpperBound()) > 0) {
            additionalTaxYearly = (grossYearly.subtract(payroll.getHigherTaxUpperBound())).multiply(payroll.getAdditionalTaxRate());
            higherTaxYearly = payroll.getHigherTaxUpperBound().subtract(payroll.getBasicTaxUpperBound()).multiply(payroll.getHigherTaxRate());
            basicTaxYearly = (payroll.getBasicTaxUpperBound().subtract(personalAllowanceYearly)).multiply(payroll.getBasicTaxRate());
        } 

        // within the higher tax bracket
        else if (grossYearly.compareTo(payroll.getHigherTaxLowerBound()) >= 0 && grossYearly.compareTo(payroll.getHigherTaxUpperBound()) <= 0) {
            additionalTaxYearly = BigDecimal.ZERO;
            higherTaxYearly = (grossYearly.subtract(payroll.getBasicTaxUpperBound())).multiply(payroll.getHigherTaxRate());
            basicTaxYearly = (payroll.getBasicTaxUpperBound().subtract(personalAllowanceYearly).multiply(payroll.getBasicTaxRate()));
        } 

        // within the basic tax bracket
        else if (grossYearly.compareTo(payroll.getBasicTaxLowerBound()) >= 0 && grossYearly.compareTo(payroll.getBasicTaxUpperBound()) <= 0) {
            additionalTaxYearly = BigDecimal.ZERO;
            higherTaxYearly = BigDecimal.ZERO;
            basicTaxYearly = (grossYearly.subtract(personalAllowanceYearly).multiply(payroll.getBasicTaxRate()));
        }
        
        // below personal allowance
        else if (grossYearly.compareTo(personalAllowanceYearly) <= 0){
            additionalTaxYearly = BigDecimal.ZERO;
            higherTaxYearly = BigDecimal.ZERO;
            basicTaxYearly = BigDecimal.ZERO;
        }
        taxYearly = (additionalTaxYearly.add(higherTaxYearly)).add(basicTaxYearly);
        taxPeriod = taxYearly.divide(period, 2, RoundingMode.HALF_EVEN);
    }
    
    /**
     * Calculates the amount the employee pays as national contributions
     * @param primaryRate - the rate the employee pays for earnings within the primary threshold
     * @param upperRate - the rate the employee pays for earnings within the upper earnings limit
     */
    private void nationalContributions(BigDecimal primaryRate, BigDecimal upperRate) {
        BigDecimal weekMultiplier = new BigDecimal("52");
        BigDecimal grossPayWeekly = grossYearly.divide(weekMultiplier, 2, RoundingMode.HALF_EVEN);

        // for earnings above the upper earnings limit
        if (grossPayWeekly.compareTo(payroll.upperEarningsLimit) > 0) {
            upperContributions = (grossPayWeekly.subtract(payroll.getUpperEarningsLimit())).multiply(upperRate);
            primaryContributions = (payroll.getUpperEarningsLimit().subtract(payroll.getPrimaryThreshold())).multiply(primaryRate);
        } // for earnings within the primary threshold
        else if (grossPayWeekly.compareTo(payroll.getPrimaryThreshold()) > 0 && grossPayWeekly.compareTo(payroll.getUpperEarningsLimit()) <= 0) {
            upperContributions = BigDecimal.ZERO;
            primaryContributions = (grossPayWeekly.subtract(payroll.getPrimaryThreshold())).multiply(primaryRate);
        } // for earnings below the primary threshold
        else if (grossPayWeekly.compareTo(payroll.getPrimaryThreshold()) <= 0) {
            upperContributions = BigDecimal.ZERO;
            primaryContributions = BigDecimal.ZERO;
        }
        nationalContributionsPeriod = upperContributions.add(primaryContributions);
        nationalContributionsYearly = nationalContributionsPeriod.multiply(weekMultiplier);
    }
    
    /**
     * Calculates the employer's national insurance contributions for the employee
     * @param primaryEmployerRate
     * @param upperEmployerRate 
     */
    private void employerContributions(BigDecimal primaryEmployerRate, BigDecimal upperEmployerRate){
        BigDecimal weekMultiplier = new BigDecimal("52");
        BigDecimal grossPayWeekly = grossYearly.divide(weekMultiplier, 2, RoundingMode.HALF_EVEN);

        // for earnings above the upper earnings limit
        if (grossPayWeekly.compareTo(payroll.upperEarningsLimit) > 0) {
            upperContributions = (grossPayWeekly.subtract(payroll.getUpperEarningsLimit())).multiply(upperEmployerRate);
            primaryContributions = (payroll.getUpperEarningsLimit().subtract(payroll.getPrimaryThreshold())).multiply(primaryEmployerRate);
        } // for earnings within the primary threshold
        else if (grossPayWeekly.compareTo(payroll.getPrimaryThreshold()) > 0 && grossPayWeekly.compareTo(payroll.getUpperEarningsLimit()) <= 0) {
            upperContributions = BigDecimal.ZERO;
            primaryContributions = (grossPayWeekly.subtract(payroll.getPrimaryThreshold())).multiply(primaryEmployerRate);
        } // for earnings below the primary threshold
        else if (grossPayWeekly.compareTo(payroll.getPrimaryThreshold()) <= 0) {
            upperContributions = BigDecimal.ZERO;
            primaryContributions = BigDecimal.ZERO;
        }
        employerContributionsPeriod = upperContributions.add(primaryContributions);
        employerContributionsYearly = employerContributionsPeriod.multiply(employerContributionsPeriod);
    }
    
    /**
     * Calculates tax paid depending on their tax code
     * @param code - the tax code the employee has
     */
    private void taxCode(String code) {
        switch (code) {
            case "NT":  // pay no tax
                break;
            case "BR":  // all income for this job taxed at basic rate
                payroll.setHigherTaxRate(payroll.getBasicTaxRate());
                payroll.setAdditionalTaxRate(payroll.getBasicTaxRate());
                personalAllowanceYear();
                taxableYearly();
                if (taxableYearly.compareTo(BigDecimal.ZERO) > 0) {
                    incomeTaxYearly();
                } else if (taxableYearly.compareTo(BigDecimal.ZERO) <= 0) {
                    taxYearly = BigDecimal.ZERO;
                    taxPeriod = BigDecimal.ZERO;
                }
                break;
            case "D0":  // all income for this job taxed at higher rate
                payroll.setBasicTaxRate(payroll.getHigherTaxRate());
                payroll.setAdditionalTaxRate(payroll.getHigherTaxRate());
                personalAllowanceYear();
                taxableYearly();
                if (taxableYearly.compareTo(BigDecimal.ZERO) > 0) {
                    incomeTaxYearly();
                } else if (taxableYearly.compareTo(BigDecimal.ZERO) <= 0) {
                    taxYearly = BigDecimal.ZERO;
                    taxPeriod = BigDecimal.ZERO;
                }
                break;
            case "D1":  // all income for this job taxed at additional rate  
                payroll.setHigherTaxRate(payroll.getAdditionalTaxRate());
                payroll.setAdditionalTaxRate(payroll.getAdditionalTaxRate());
                personalAllowanceYear();
                taxableYearly();
                if (taxableYearly.compareTo(BigDecimal.ZERO) > 0) {
                    incomeTaxYearly();
                } else if (taxableYearly.compareTo(BigDecimal.ZERO) <= 0) {
                    taxYearly = BigDecimal.ZERO;
                    taxPeriod = BigDecimal.ZERO;
                }
                break;
            case "M":   // received 10% of partner's personal allowance
                personalAllowanceYear();
                personalAllowanceYearly = personalAllowanceYearly.add((partnersPAYearly.divide(BigDecimal.TEN, RoundingMode.HALF_EVEN)));
                taxableYearly();
                if (taxableYearly.compareTo(BigDecimal.ZERO) > 0) {
                    incomeTaxYearly();
                } else if (taxableYearly.compareTo(BigDecimal.ZERO) <= 0) {
                    taxYearly = BigDecimal.ZERO;
                    taxPeriod = BigDecimal.ZERO;
                }
                break;
            case "N":   // transferred 10% of personal allowance to partner
                personalAllowanceYear();
                personalAllowanceYearly = personalAllowanceYearly.subtract((personalAllowanceYearly.divide(BigDecimal.TEN, RoundingMode.HALF_EVEN)));
                taxableYearly();
                if (taxableYearly.compareTo(BigDecimal.ZERO) > 0) {
                    incomeTaxYearly();
                } else if (taxableYearly.compareTo(BigDecimal.ZERO) <= 0) {
                    taxYearly = BigDecimal.ZERO;
                    taxPeriod = BigDecimal.ZERO;
                }
                break;
            case "T":   // earnings are £150,000+ but still has some personal allowance
                personalAllowanceYear();
                taxableYearly();
                if (taxableYearly.compareTo(BigDecimal.ZERO) > 0) {
                    incomeTaxYearly();
                } else if (taxableYearly.compareTo(BigDecimal.ZERO) <= 0) {
                    taxYearly = BigDecimal.ZERO;
                    taxPeriod = BigDecimal.ZERO;
                }
                break;
            case "0T":  // earnings are £150,000+ and has no personal allowance
                personalAllowanceYearly = BigDecimal.ZERO;
                taxableYearly();
                if (taxableYearly.compareTo(BigDecimal.ZERO) > 0) {
                    incomeTaxYearly();
                } else if (taxableYearly.compareTo(BigDecimal.ZERO) <= 0) {
                    taxYearly = BigDecimal.ZERO;
                    taxPeriod = BigDecimal.ZERO;
                }
                break;
            case "L":   // standard personal allowance
                personalAllowanceYear();
                taxableYearly();
                if (taxableYearly.compareTo(BigDecimal.ZERO) > 0) {
                    incomeTaxYearly();
                } else if (taxableYearly.compareTo(BigDecimal.ZERO) <= 0) {
                    taxYearly = BigDecimal.ZERO;
                    taxPeriod = BigDecimal.ZERO;
                }
                break;
        }
    }
    
    /**
     * Decides what rates to use when calculating the employee's national insurance contributions
     * @param category - the national insurance category that the employee belongs to
     */
    private void nationalCategory(String category){
        switch (category){
            case "A":
                nationalContributions(payroll.getCategoryAPrimaryRate(), payroll.getCategoryAUpperRate());
                employerContributions(payroll.getCategoryASecondaryRate(), payroll.getCategoryAUpperSecondaryRate());
                break;
            case "B":
                nationalContributions(payroll.getCategoryBPrimaryRate(), payroll.getCategoryBUpperRate());
                employerContributions(payroll.getCategoryBSecondaryRate(), payroll.getCategoryBUpperSecondaryRate());
                break;
            case "C":
                nationalContributions(payroll.getCategoryCPrimaryRate(), payroll.getCategoryCUpperRate());
                employerContributions(payroll.getCategoryCSecondaryRate(), payroll.getCategoryCUpperSecondaryRate());
                break;
            case "H":
                nationalContributions(payroll.getCategoryHPrimaryRate(), payroll.getCategoryHUpperRate());
                employerContributions(payroll.getCategoryHSecondaryRate(), payroll.getCategoryHUpperSecondaryRate());
                break;
            case "J":
                nationalContributions(payroll.getCategoryJPrimaryRate(), payroll.getCategoryJUpperRate());
                employerContributions(payroll.getCategoryJSecondaryRate(), payroll.getCategoryJUpperSecondaryRate());
                break;
            case "M":
                nationalContributions(payroll.getCategoryMPrimaryRate(), payroll.getCategoryMUpperRate());
                employerContributions(payroll.getCategoryMSecondaryRate(), payroll.getCategoryMUpperSecondaryRate());
                break;
            case "Z":
                nationalContributions(payroll.getCategoryZPrimaryRate(), payroll.getCategoryZUpperRate());
                employerContributions(payroll.getCategoryZSecondaryRate(), payroll.getCategoryZUpperSecondaryRate());
                break;
        }
    }
    
    /**
     * Calculates the take home pay of the employee
     */
    private void netPayYearly() {
        BigDecimal period = new BigDecimal("52");
        BigDecimal totalAdditionalPostTax = additionalPayPostTax.subtract(additionalDeductionsPostTax);
        netYearly = grossYearly.subtract(taxYearly.add(nationalContributionsYearly));
        netPeriod = (netYearly.divide(period, 2, RoundingMode.HALF_EVEN)).add(totalAdditionalPostTax);
    }
    
    private void yearPay(){
    BigDecimal grossYearCurrent = new BigDecimal(employee.getYearGross());
    BigDecimal taxableYearCurrent = new BigDecimal(employee.getYearTaxable());
    BigDecimal taxYearCurrent = new BigDecimal(employee.getYearIncomeTax());
    BigDecimal contributionsYearCurrent = new BigDecimal(employee.getYearNationalContributions());
    BigDecimal employerContributionsYearCurrent = new BigDecimal(employee.getYearEmployerContributions());
    BigDecimal netYearCurrent = new BigDecimal(employee.getYearGross());
    
    grossYearUpdated = grossYearCurrent.add(grossPeriod);
    taxableYearUpdated = taxableYearCurrent.add(taxablePeriod);;
    taxYearUpdated = taxYearCurrent.add(taxPeriod);
    contributionsYearUpdated = contributionsYearCurrent.add(nationalContributionsPeriod);
    employerContributionsYearUpdated = employerContributionsYearCurrent.add(employerContributionsPeriod);
    netYearUpdated = netYearCurrent.add(netPeriod);
    
    }
    /**
     * Calculates the values for payroll for the employee
     * @param employeeId - the id for the employee to process
     */
    public void calculatePay(String employeeId){
        quantityThisPeriod(employeeId);
        BigDecimal payRate;
        BigDecimal basicQuantity = new BigDecimal(basicQuantityStr);
        employee.searchEmployment(employeeId);
        payRate = new BigDecimal(employee.getPayRate());
        payroll.setStandardPayrollValues();
        grossPayYearly(payRate, basicQuantity);       
        taxCode(employee.getTaxCode());
        nationalCategory(employee.getNationalCategory());
        netPayYearly();
        yearPay();
        calculated = true;
    }
    
    /**
     * obtains the number of hours this employee worked this period
     * @param employeeId 
     */
    private void quantityThisPeriod(String employeeId) {
        employee.searchEmployee(employeeId);
        ArrayList<String> transactionArrayList = new ArrayList<String>();
        String[][] transactionArray2d;  // stores current line in file
        String transactionLine;
        String splitBy = ",";   // splits line by commas into an array
        boolean foundFirstId = false;
        boolean foundLastId = false;
        int i;
        int currentIdFirstPtr = 0;
        int currentIdLastPtr = 0;
        BigDecimal quantity = BigDecimal.ZERO;  // the amount of hours this employee has worked
        BigDecimal hours;   // number of hours this employee has worked
        BigDecimal basicRate = new BigDecimal(employee.getPayRate());
        BigDecimal amount;
        try {
            // create reader object
            FileReader transactionReader = new FileReader(transactionFile);
            BufferedReader transactionBuff = new BufferedReader(transactionReader);
            transactionBuff.readLine(); // skip headers

            while ((transactionLine = transactionBuff.readLine()) != null) {    // repeats until end of file
                transactionArrayList.add(transactionLine); // store line to array list
            }
            transactionReader.close();  // close reader
            // convert transactionArrayList to 2d array
            transactionArray2d = new String[transactionArrayList.size()][transactionArrayList.get(0).length()];
            for (i = 0; i < transactionArrayList.size(); i++) {
                transactionArray2d[i] = transactionArrayList.get(i).split(splitBy);
            }

            for (i = 0; i < transactionArray2d.length; i++) {
                // finds first instance of employee id
                if (employeeId.equals(transactionArray2d[i][0]) && foundFirstId == false) {
                    currentIdFirstPtr = i;
                    foundFirstId = true;
                }
                // finds last instance of employee id
                if (foundFirstId == true) {
                    if (foundLastId == false) {
                        if (employeeId.equals(transactionArray2d[i][0])) {
                            currentIdLastPtr = i;
                        } else {
                            foundLastId = true;
                        }
                    }
                }
            }
            // finds all instances of employee id
            while (employeeId.equals(transactionArray2d[currentIdLastPtr][0])) {
                currentIdLastPtr = currentIdLastPtr + 1;    // increment the pointer
            }
            // calculate total hours worked
            for (i = currentIdFirstPtr; i <= currentIdLastPtr - 1; i++) {
                hours = new BigDecimal(transactionArray2d[i][2]);
                quantity = quantity.add(hours);
            }
            basicQuantityStr = quantity.toString();
            amount = quantity.multiply(basicRate);
            basicAmountStr = amount.setScale(2, RoundingMode.HALF_EVEN).toString();

        } catch (IOException ex) {
            System.out.println("Error reading " + transactionFile);
        }
    }
    
    public String getBasicQuantityStr(){
        return this.basicQuantityStr;
    }
    public String getBasicAmountStr(){
        return this.basicAmountStr;
    }
    
    public BigDecimal getGrossPeriod(){
        return this.grossPeriod;
    }
    
    public BigDecimal getTaxablePeriod(){
        return this.taxablePeriod;
    }
    
    public BigDecimal getTaxPeriod(){
        return this.taxPeriod;
    }
    
    public BigDecimal getNationalContributionsPeriod(){
        return this.nationalContributionsPeriod;
    }
    
    public BigDecimal getEmployerContributionsPeriod(){
        return this.employerContributionsPeriod;
    }
    
    public BigDecimal getNetPeriod(){
        return this.netPeriod;
    }
    
    public String getGrossPeriodStr(){
        return this.grossPeriod.setScale(2, RoundingMode.HALF_EVEN).toString();
    }
    
    public String getTaxablePeriodStr(){
        return this.taxablePeriod.setScale(2, RoundingMode.HALF_EVEN).toString();
    }
    
    public String getTaxPeriodStr(){
        return this.taxPeriod.setScale(2, RoundingMode.HALF_EVEN).toString();
    }
    
    public String getNationalContributionsPeriodStr(){
        return this.nationalContributionsPeriod.setScale(2, RoundingMode.HALF_EVEN).toString();
    }
    
    public String getEmployerContributionsPeriodStr(){
        return this.employerContributionsPeriod.setScale(2, RoundingMode.HALF_EVEN).toString();
    }
    
    public String getNetPeriodStr(){
        return this.netPeriod.setScale(2, RoundingMode.HALF_EVEN).toString();
    }
    
    public String getGrossYearUpdatedStr(){
        return this.grossYearUpdated.setScale(2, RoundingMode.HALF_EVEN).toString();
    }
    
    public String getTaxableYearUpdatedStr(){
        return this.taxableYearUpdated.setScale(2, RoundingMode.HALF_EVEN).toString();
    }
    
    public String getTaxYearUpdatedStr(){
        return this.taxYearUpdated.setScale(2, RoundingMode.HALF_EVEN).toString();
    }
    
    public String getNationalContributionsYearUpdatedStr(){
        return this.contributionsYearUpdated.setScale(2, RoundingMode.HALF_EVEN).toString();
    }
    
    public String getEmployerContributionsYearUpdatedStr(){
        return this.employerContributionsYearUpdated.setScale(2, RoundingMode.HALF_EVEN).toString();
    }
    
    public String getNetYearUpdatedStr(){
        return this.netYearUpdated.setScale(2, RoundingMode.HALF_EVEN).toString();
    }
}
