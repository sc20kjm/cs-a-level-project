/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author MAD16833376
 */
public class Employee {

    final String employeeFileDir = "Employee Files\\"; // employee files location
    final String systemFileDir = "System Files\\";   // system files location
    final String leaversDir = employeeFileDir + "Leavers\\";

    final String loginFileName = "Login Details";   // login file name
    final String securityFileName = "Security Details"; // security file name
    final String personalDetailsFileName = "Personal Details";  // personal details file name
    final String employmentDetailsFileName = "Employment Details";  // employment details file name
    final String departmentDetailsFileName = "Department Details";  // department details file name

    final File loginFile = new File(employeeFileDir + loginFileName + ".csv");  // login file
    final File securityFile = new File(employeeFileDir + securityFileName + ".csv");        // security file
    final File personalDetailsFile = new File(employeeFileDir + personalDetailsFileName + ".csv");  // personal details file
    final File employmentDetailsFile = new File(employeeFileDir + employmentDetailsFileName + ".csv");  // employment details file
    final File departmentDetailsFile = new File(employeeFileDir + departmentDetailsFileName + ".csv");  // department details file
    final File leaverPersonalDetailsFile = new File(leaversDir + personalDetailsFileName + ".csv");  // personal details file
    final File leaverEmploymentDetailsFile = new File(leaversDir + employmentDetailsFileName + ".csv");  // employment details file

    private String employeeId;

    private String password;
    private String accessLevel;
    private String loginRecord;

    private String securityQ1;
    private String securityQ2;
    private String securityQ3;
    private String answer1;
    private String answer2;
    private String answer3;
    private String securityRecord;

    private String forename;
    private String surname;
    private String title;
    private String dob;
    private String gender;
    private String address;
    private String postcode;
    private String email;
    private String telephone;
    private String mobile;
    private String personalRecord;

    private String updatedForename;
    private String updatedSurname;
    private String updatedTitle;
    private String updatedDob;
    private String updatedGender;
    private String updatedAddress;
    private String updatedPostcode;
    private String updatedEmail;
    private String updatedTelephone;
    private String updatedMobile;
    private String updatedPersonalRecord;

    private String status;
    private String departmentNum;
    private String department;
    private String managerId;
    private String managerForename;
    private String managerSurname;
    private String payRate;
    private String nationalCategory;
    private String taxCode;
    private String lastCalculated;
    private String lastGrossStr;
    private String lastTaxableStr;
    private String lastIncomeTaxStr;
    private String lastNationalContributionStr;
    private String lastEmployerContributionStr;
    private String lastNetStr;
    private String yearGrossStr;
    private String yearTaxableStr;
    private String yearIncomeTaxStr;
    private String yearNationalContributionStr;
    private String yearEmployerContributionStr;
    private String yearNetStr;
    private String employmentRecord;

    private String updatedStatus;
    private String updatedDepartmentNum;
    private String updatedDepartment;
    private String updatedManagerId;
    private String updatedManagerForename;
    private String updatedManagerSurname;
    private String updatedPayRate;
    private String updatedNationalCategory;
    private String updatedTaxCode;
    private String updatedLastCalculated;
    private String updatedLastGrossStr;
    private String updatedLastTaxableStr;
    private String updatedLastIncomeTaxStr;
    private String updatedLastNationalContributionStr;
    private String updatedLastEmployerContributionStr;
    private String updatedLastNetStr;
    private String updatedYearGrossStr;
    private String updatedYearTaxableStr;
    private String updatedYearIncomeTaxStr;
    private String updatedYearNationalContributionStr;
    private String updatedYearEmployerContributionStr;
    private String updatedYearNetStr;
    private String updatedEmploymentRecord;

    public boolean isEmployeeFound = true;

    public void setEmployeeID(String employeeId) {
        this.employeeId = employeeId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    public void setLoginRecord(String loginRecord) {
        this.loginRecord = loginRecord;
        String loginArray[] = loginRecord.split(",");
        setEmployeeID(loginArray[0]);
        setPassword(loginArray[1]);
        setAccessLevel(loginArray[2]);
    }

    public void setSecurityQ1(String securityQ1) {
        this.securityQ1 = securityQ1;
    }

    public void setSecurityQ2(String securityQ2) {
        this.securityQ2 = securityQ2;
    }

    public void setSecurityQ3(String securityQ3) {
        this.securityQ3 = securityQ3;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public void setSecurityRecord(String securityRecord) {
        this.securityRecord = securityRecord;
        String[] securityArray = securityRecord.split(",");
        setEmployeeID(securityArray[0]);
        setSecurityQ1(securityArray[1]);
        setSecurityQ2(securityArray[2]);
        setSecurityQ3(securityArray[3]);
        setAnswer1(securityArray[4]);
        setAnswer2(securityArray[5]);
        setAnswer3(securityArray[6]);
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDOB(String dob) {
        this.dob = dob;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setPersonalRecord(String personalRecord) {
        this.personalRecord = personalRecord;
        String personalArr[] = personalRecord.split(",");
        setEmployeeID(personalArr[0]);
        setTitle(personalArr[1]);
        setForename(personalArr[2]);
        setSurname(personalArr[3]);
        setGender(personalArr[4]);
        setDOB(personalArr[5]);
        setAddress(personalArr[6]);
        setPostcode(personalArr[7]);
        setEmail(personalArr[8]);
        setTelephone(personalArr[9]);
        setMobile(personalArr[10]);
    }

    public void setUpdatedForename(String updatedForename) {
        this.updatedForename = updatedForename;
    }

    public void setUpdatedSurname(String updatedSurname) {
        this.updatedSurname = updatedSurname;
    }

    public void setUpdatedTitle(String updatedTitle) {
        this.updatedTitle = updatedTitle;
    }

    public void setUpdatedDOB(String updatedDob) {
        this.updatedDob = updatedDob;
    }

    public void setUpdatedGender(String updatedGender) {
        this.updatedGender = updatedGender;
    }

    public void setUpdatedAddress(String updatedAddress) {
        this.updatedAddress = updatedAddress;
    }

    public void setUpdatedPostcode(String updatedPostcode) {
        this.updatedPostcode = updatedPostcode;
    }

    public void setUpdatedEmail(String updatedEmail) {
        this.updatedEmail = updatedEmail;
    }

    public void setUpdatedTelephone(String updatedTelephone) {
        this.updatedTelephone = updatedTelephone;
    }

    public void setUpdatedMobile(String updatedMobile) {
        this.updatedMobile = updatedMobile;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDepartmentNum(String departmentNum) {
        this.departmentNum = departmentNum;
    }

    public void setDepartmentDetails(String departmentNum) {
        boolean deptFound = false;
        String line;
        String[] deptRecord;
        String splitBy = ",";
        try {
            // create reader object
            FileReader deptRead = new FileReader(departmentDetailsFile);
            BufferedReader deptBuff = new BufferedReader(deptRead);
            deptBuff.readLine();    // skip headers

            while ((line = deptBuff.readLine()) != null && deptFound == false) {
                deptRecord = line.split(splitBy);
                if (deptRecord[0].equals(getDepartmentNum())) {
                    this.department = deptRecord[1];
                    this.managerId = deptRecord[2];
                    getManagerName();
                    deptFound = true;
                }
            }
            deptRead.close();
            deptBuff.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void getManagerName() {
        boolean found = false;
        String line;    // string to temporarily store line read from file
        String[] record = null;
        try {
            // Create file reader object
            FileReader personalReader = new FileReader(personalDetailsFile);
            BufferedReader personalBuff = new BufferedReader(personalReader);

            // Loop through each record of the file until search item found or end of file reached
            while (((line = personalBuff.readLine()) != null) && (found == false)) {
                record = line.split(",");    // array holds last line read

                // checks if employee id matches
                if (record[0].equals(managerId)) {
                    // set manager forename and surname
                    managerForename = record[2];
                    managerSurname = record[3];
                    // set found to true to stop looping
                    found = true;
                }
            }
            personalReader.close();
            personalBuff.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setPayRate(String payRate) {
        this.payRate = payRate;
    }

    public void setNationalCategory(String nationalCategory) {
        this.nationalCategory = nationalCategory;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public void setLastCalculated(String lastCalculated) {
        this.lastCalculated = lastCalculated;
    }

    public void setLastGross(String lastGross) {
        this.lastGrossStr = lastGross;
    }

    public void setLastTaxable(String lastTaxable) {
        this.lastTaxableStr = lastTaxable;
    }

    public void setLastIncomeTax(String lastIncomeTax) {
        this.lastIncomeTaxStr = lastIncomeTax;
    }

    public void setLastNationalContribution(String lastNationalContribution) {
        this.lastNationalContributionStr = lastNationalContribution;
    }

    public void setLastEmployerContribution(String lastEmployerContribution) {
        this.lastEmployerContributionStr = lastEmployerContribution;
    }

    public void setLastNet(String lastNet) {
        this.lastNetStr = lastNet;
    }

    public void setYearGross(String yearGross) {
        this.yearGrossStr = yearGross;
    }

    public void setYearTaxable(String yearTaxable) {
        this.yearTaxableStr = yearTaxable;
    }

    public void setYearIncomeTax(String yearIncomeTax) {
        this.yearIncomeTaxStr = yearIncomeTax;
    }

    public void setYearNationalContribution(String yearNationalContribution) {
        this.yearNationalContributionStr = yearNationalContribution;
    }

    public void setYearEmployerContribution(String yearEmployerContribution) {
        this.yearEmployerContributionStr = yearEmployerContribution;
    }

    public void setYearNet(String yearNet) {
        this.yearNetStr = yearNet;
    }

    public void setEmploymentRecord(String employmentRecord) {
        this.employmentRecord = employmentRecord;
        String employmentArr[] = employmentRecord.split(",");
        setDepartmentNum(employmentArr[1]);
        setPayRate(employmentArr[2]);
        setNationalCategory(employmentArr[3]);
        setTaxCode(employmentArr[4]);
        setLastCalculated(employmentArr[5]);
        setLastGross(employmentArr[6]);
        setLastTaxable(employmentArr[7]);
        setLastIncomeTax(employmentArr[8]);
        setLastNationalContribution(employmentArr[9]);
        setLastEmployerContribution(employmentArr[10]);
        setLastNet(employmentArr[11]);
        setYearGross(employmentArr[12]);
        setYearTaxable(employmentArr[13]);
        setYearIncomeTax(employmentArr[14]);
        setYearNationalContribution(employmentArr[15]);
        setYearEmployerContribution(employmentArr[16]);
        setYearNet(employmentArr[17]);
    }

    public void setUpdatedStatus(String updatedStatus) {
        this.updatedStatus = updatedStatus;
    }

    public void setUpdatedDepartmentNum(String updatedDepartmentNum) {
        this.updatedDepartmentNum = updatedDepartmentNum;
    }

    public void setUpdatedDepartmentDetails(String updatedDepartmentNum) {
        String line;
        String[] deptRecord;
        String splitBy = ",";
        try {
            // create reader object
            FileReader deptRead = new FileReader(departmentDetailsFile);
            BufferedReader deptBuff = new BufferedReader(deptRead);
            deptBuff.readLine();    // skip headers

            while ((line = deptBuff.readLine()) != null) {
                deptRecord = line.split(splitBy);
                if (deptRecord[0].equals(getDepartmentNum())) {
                    this.department = deptRecord[1];
                    this.managerId = deptRecord[2];
                    getManagerName();
                }
            }
            deptRead.close();
            deptBuff.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setUpdatedPayRate(String updatedPayRate) {
        this.updatedPayRate = updatedPayRate;
    }

    public void setUpdatedNationalCategory(String updatedNationalCategory) {
        this.updatedNationalCategory = updatedNationalCategory;
    }

    public void setUpdatedTaxCode(String updatedTaxCode) {
        this.updatedTaxCode = updatedTaxCode;
    }

    public void setUpdatedLastCalculated(String updatedLastCalculated) {
        this.updatedLastCalculated = updatedLastCalculated;
    }

    public void setUpdatedLastGross(String updatedLastGrossStr) {
        this.updatedLastGrossStr = updatedLastGrossStr;
    }

    public void setUpdatedLastTaxable(String updatedLastTaxableStr) {
        this.updatedLastTaxableStr = updatedLastTaxableStr;
    }

    public void setUpdatedLastIncomeTax(String updatedLastIncomeTaxStr) {
        this.updatedLastIncomeTaxStr = updatedLastIncomeTaxStr;
    }

    public void setUpdatedLastNationalContribution(String updatedLastNationalContributionStr) {
        this.updatedLastNationalContributionStr = updatedLastNationalContributionStr;
    }

    public void setUpdatedLastEmployerContribution(String updatedLastEmployerContributionStr) {
        this.updatedLastEmployerContributionStr = updatedLastEmployerContributionStr;
    }

    public void setUpdatedLastNet(String updatedLastNetStr) {
        this.updatedLastNetStr = updatedLastNetStr;
    }

    public void setUpdatedYearGross(String updatedYearGrossStr) {
        this.updatedYearGrossStr = updatedYearGrossStr;
    }

    public void setUpdatedYearTaxable(String updatedYearTaxableStr) {
        this.updatedYearTaxableStr = updatedYearTaxableStr;
    }

    public void setUpdatedYearIncomeTax(String updatedYearIncomeTaxStr) {
        this.updatedYearIncomeTaxStr = updatedYearIncomeTaxStr;
    }

    public void setUpdatedYearNationalContribution(String updatedYearNationalContributionStr) {
        this.updatedYearNationalContributionStr = updatedYearNationalContributionStr;
    }

    public void setUpdatedYearEmployerContribution(String updatedYearEmployerContributionStr) {
        this.updatedYearEmployerContributionStr = updatedYearEmployerContributionStr;
    }

    public void setUpdatedYearNet(String updatedYearNetStr) {
        this.updatedYearNetStr = updatedYearNetStr;
    }

    public String getEmployeeID() {
        return this.employeeId;
    }

    public String getPassword() {
        return this.password;
    }

    public String getAccessLevel() {
        return this.accessLevel;
    }

    public String getLoginRecord() {
        loginRecord = employeeId + "," + password + "," + accessLevel;
        return this.loginRecord;
    }

    public String getSecurityQ1() {
        return this.securityQ1;
    }

    public String getSecurityQ2() {
        return this.securityQ2;
    }

    public String getSecurityQ3() {
        return this.securityQ3;
    }

    public String getAnswer1() {
        return this.answer1;
    }

    public String getAnswer2() {
        return this.answer2;
    }

    public String getAnswer3() {
        return this.answer3;
    }

    public String getSecurityRecord() {
        securityRecord = employeeId + "," + securityQ1 + "," + securityQ2 + "," + securityQ3 + "," + answer1 + "," + answer2 + "," + answer3;
        return this.securityRecord;
    }

    public String getForename() {
        return this.forename;
    }

    public String getSurname() {
        return this.surname;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDOB() {
        return this.dob;
    }

    public String getGender() {
        return this.gender;
    }

    public String getAddress() {
        return this.address;
    }

    public String getPostcode() {
        return this.postcode;
    }

    public String getEmail() {
        return this.email;
    }

    public String getTel() {
        return this.telephone;
    }

    public String getMobile() {
        return this.mobile;
    }

    public String getPersonalRecord() {
        personalRecord = employeeId + "," + title + "," + forename + "," + surname + "," + gender + "," + dob + "," + address
                + "," + postcode + "," + email + "," + telephone + "," + mobile;
        return this.personalRecord;
    }

    public String getUpdatedForename() {
        return this.updatedForename;
    }

    public String getUpdatedSurname() {
        return this.updatedSurname;
    }

    public String getUpdatedTitle() {
        return this.updatedTitle;
    }

    public String getUpdatedDOB() {
        return this.updatedDob;
    }

    public String getUpdatedGender() {
        return this.updatedGender;
    }

    public String getUpdatedAddress() {
        return this.updatedAddress;
    }

    public String getUpdatedPostcode() {
        return this.updatedPostcode;
    }

    public String getUpdatedEmail() {
        return this.updatedEmail;
    }

    public String getUpdatedTelephone() {
        return this.updatedTelephone;
    }

    public String getUpdatedMobile() {
        return this.updatedMobile;
    }

    public String getUpdatedPersonalRecord() {
        updatedPersonalRecord = employeeId + "," + updatedTitle + "," + updatedForename + "," + updatedSurname + "," + updatedGender
                + "," + updatedDob + "," + updatedAddress + "," + updatedPostcode
                + "," + updatedEmail + "," + updatedTelephone + "," + updatedMobile;
        return this.updatedPersonalRecord;
    }

    public String getStatus() {
        return this.status;
    }

    public String getDepartmentNum() {
        return this.departmentNum;
    }

    public String getDepartment() {
        return this.department;
    }

    public String getManagerId() {
        return this.managerId;
    }

    public String getManagerForename() {
        return this.managerForename;
    }

    public String getManagerSurname() {
        return this.managerSurname;
    }

    public String getPayRate() {
        return this.payRate;
    }

    public String getNationalCategory() {
        return this.nationalCategory;
    }

    public String getTaxCode() {
        return this.taxCode;
    }

    public String getLastCalculated() {
        return this.lastCalculated;
    }

    public String getLastGross() {
        return this.lastGrossStr;
    }

    public String getLastTaxable() {
        return this.lastTaxableStr;
    }

    public String getLastIncomeTax() {
        return this.lastIncomeTaxStr;
    }

    public String getLastNationalContributions() {
        return this.lastNationalContributionStr;
    }

    public String getLastEmployerContributions() {
        return this.lastEmployerContributionStr;
    }

    public String getLastNet() {
        return this.lastNetStr;
    }

    public String getYearGross() {
        return this.yearGrossStr;
    }

    public String getYearTaxable() {
        return this.yearTaxableStr;
    }

    public String getYearIncomeTax() {
        return this.yearIncomeTaxStr;
    }

    public String getYearNationalContributions() {
        return this.yearNationalContributionStr;
    }

    public String getYearEmployerContributions() {
        return this.yearEmployerContributionStr;
    }

    public String getYearNet() {
        return this.yearNetStr;
    }

    public String getEmploymentRecord() {
        employmentRecord = employeeId + "," + status + "," + departmentNum + "," + payRate + "," + nationalCategory
                + "," + taxCode + "," + lastCalculated + "," + lastGrossStr + "," + lastTaxableStr
                + "," + lastIncomeTaxStr + "," + lastNationalContributionStr + "," + lastEmployerContributionStr + "," + lastNetStr + "," + yearGrossStr
                + "," + yearTaxableStr + "," + yearIncomeTaxStr + "," + yearNationalContributionStr + "," + yearEmployerContributionStr + "," + yearNetStr;
        return employmentRecord;
    }

    public String getUpdatedEmploymentDetailsRecord() {
        employmentRecord = employeeId + "," + updatedStatus + "," + updatedDepartmentNum + "," + updatedPayRate + "," + updatedNationalCategory
                + "," + updatedTaxCode + "," + lastCalculated + "," + lastGrossStr + "," + lastTaxableStr
                + "," + lastIncomeTaxStr + "," + lastNationalContributionStr + "," + lastEmployerContributionStr + "," + lastNetStr + "," + yearGrossStr
                + "," + yearTaxableStr + "," + yearIncomeTaxStr + "," + yearNationalContributionStr + "," + yearEmployerContributionStr + "," + yearNetStr;
        return employmentRecord;
    }

    public String getUpdatedEmploymentPayRecord() {
        employmentRecord = employeeId + "," + status + "," + departmentNum + "," + payRate + "," + nationalCategory
                + "," + taxCode + "," + updatedLastCalculated + "," + updatedLastGrossStr + "," + updatedLastTaxableStr
                + "," + updatedLastIncomeTaxStr + "," + updatedLastNationalContributionStr + "," + updatedLastEmployerContributionStr + "," + updatedLastNetStr + "," + updatedYearGrossStr
                + "," + updatedYearTaxableStr + "," + updatedYearIncomeTaxStr + "," + updatedYearNationalContributionStr + "," + updatedYearEmployerContributionStr + "," + updatedYearNetStr;
        return employmentRecord;
    }

    public String getUpdatedStatus() {
        return this.updatedStatus;
    }

    public String getUpdatedDepartmentNum() {
        return this.updatedDepartmentNum;
    }

    public String getUpdatedDepartment() {
        return this.updatedDepartmentNum;
    }

    public String getUpdatedManagerId() {
        return this.managerId;
    }

    public String getUpdatedManagerForename() {
        return this.managerForename;
    }

    public String getUpdatedManagerSurname() {
        return this.managerSurname;
    }

    public String getUpdatedPayRate() {
        return this.updatedPayRate;
    }

    public String getUpdatedNationalCategory() {
        return this.updatedNationalCategory;
    }

    public String getUpdatedTaxCode() {
        return this.updatedTaxCode;
    }

    public String getUpdatedLastCalculated() {
        return this.updatedLastCalculated;
    }

    public String getUpdatedLastGross() {
        return this.updatedLastGrossStr;
    }

    public String getUpdatedLastTaxable() {
        return this.updatedLastTaxableStr;
    }

    public String getUpdatedLastIncomeTax() {
        return this.updatedLastIncomeTaxStr;
    }

    public String getUpdatedLastNationalContributions() {
        return this.updatedLastNationalContributionStr;
    }

    public String getUpdatedLastEmployerContributions() {
        return this.updatedLastEmployerContributionStr;
    }

    public String getUpdatedLastNet() {
        return this.updatedLastNetStr;
    }

    public String getUpdatedYearGross() {
        return this.updatedYearGrossStr;
    }

    public String getUpdatedYearTaxable() {
        return this.updatedYearTaxableStr;
    }

    public String getUpdatedYearIncomeTax() {
        return this.updatedYearIncomeTaxStr;
    }

    public String getUpdatedYearNationalContributions() {
        return this.updatedYearNationalContributionStr;
    }

    public String getUpdatedYearEmployerContributions() {
        return this.updatedYearEmployerContributionStr;
    }

    public String getUpdatedYearNet() {
        return this.updatedYearNetStr;
    }

    public String generateEmployeeId() {
        String prefix = surname.substring(0, 3).toUpperCase();

        int min = 10000;
        int max = 99999;

        int idNum = min + (int) (Math.random() * ((max - min) + 1));

        if (!prefix.equals("")) {
            employeeId = prefix + idNum;
        }
        return employeeId;
    }

    /**
     * adds new employee details to corresponding files
     */
    public void addEmployee() {
        generateEmployeeId();

        setPassword(employeeId + "PASS");

        setSecurityQ1("0");
        setSecurityQ2("1");
        setSecurityQ3("2");
        setAnswer1("answer1");
        setAnswer2("answer2");
        setAnswer3("answer3");

        setStatus("Active");
        setLastGross("0.00");
        setLastTaxable("0.00");
        setLastIncomeTax("0.00");
        setLastNationalContribution("0.00");
        setLastEmployerContribution("0.00");
        setLastNet("0.00");
        setYearGross("0.00");
        setYearTaxable("0.00");
        setYearIncomeTax("0.00");
        setYearNationalContribution("0.00");
        setYearEmployerContribution("0.00");
        setYearNet("0.00");

        try {
            // Create file writer objects
            FileWriter loginWriter = new FileWriter(loginFile, true);
            FileWriter securityWriter = new FileWriter(securityFile, true);
            FileWriter personalWriter = new FileWriter(personalDetailsFile, true);
            FileWriter employeeWriter = new FileWriter(employmentDetailsFile, true);
            // write record to files plus new line
            loginWriter.write("\n" + getLoginRecord());
            securityWriter.write("\n" + getSecurityRecord());
            personalWriter.write("\n" + getPersonalRecord());
            employeeWriter.write("\n" + getEmploymentRecord());
            loginWriter.close();
            securityWriter.close();
            personalWriter.close();
            employeeWriter.close();
            // show success message
            JOptionPane.showMessageDialog(null, "Save Successful", "Employee Data", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
            System.out.println("Error adding new employee");
        }
    }

    /**
     * edits an employee's personal record
     *
     * @param employeeId - id of employee to edit
     * @return
     */
    public boolean editPersonalFile(String employeeId) {
        boolean success = true;    // indicates whether edit was successful or not
        File tempFile = new File("tempPersonalFile.csv");
        String readLine; // string to temporarily store line read from file
        String splitBy = ",";
        String[] record;
        try {
            // Create file reader object
            FileReader reader = new FileReader(personalDetailsFile);
            BufferedReader buffReader = new BufferedReader(reader);

            FileWriter writer = new FileWriter(tempFile);
            writer.write(buffReader.readLine());    // write headers to temp file

            // Loop through each record of the file until end of file reached
            while (((readLine = buffReader.readLine()) != null)) {
                record = readLine.split(splitBy);
                // if employee id matches current record's id
                if (employeeId.equals(record[0])) {
                    // write the updated record to temp file
                    writer.write("\n" + getUpdatedPersonalRecord());
                } else {
                    // otherwise write the original record
                    writer.write("\n" + readLine);
                }
            }

            // close writer and reader
            reader.close();
            buffReader.close();
            writer.close();
            // delete old file
            if (personalDetailsFile.delete()) {
                // rename temp file
                if (!tempFile.renameTo(personalDetailsFile)) {
                    success = false;
                }
            } else {
                success = false;
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            success = false;
        } catch (IOException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            success = false;
        }
        return success;
    }

    /**
     * edits an employee's employment record with new employment details
     *
     * @param employeeId - id of employee to edit
     * @return
     */
    public boolean editEmploymentFileDetails(String employeeId) {
        boolean success = true;    // indicates whether edit was successful or not
        File tempFile = new File("tempPersonalFile.csv");
        String readLine; // string to temporarily store line read from file
        String splitBy = ",";
        String[] record;
        try {
            // Create file reader object
            FileReader reader = new FileReader(employmentDetailsFile);
            BufferedReader buffReader = new BufferedReader(reader);

            FileWriter writer = new FileWriter(tempFile);
            writer.write(buffReader.readLine());    // write headers to temp file

            // Loop through each record of the file until end of file reached
            while (((readLine = buffReader.readLine()) != null)) {
                record = readLine.split(splitBy);
                // if employee id matches current record's id
                if (employeeId.equals(record[0])) {
                    // write the updated record to temp file
                    writer.write("\n" + getUpdatedEmploymentDetailsRecord());
                } else {
                    // otherwise write the original record
                    writer.write("\n" + readLine);
                }
            }

            // close writer and reader
            reader.close();
            buffReader.close();
            writer.close();
            // delete old file
            if (employmentDetailsFile.delete()) {
                // rename temp file
                if (!tempFile.renameTo(employmentDetailsFile)) {
                    success = false;
                }
            } else {
                success = false;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            success = false;
        } catch (IOException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            success = false;
        }
        return success;
    }

    /**
     * edits an employee's employment record with new pay details
     *
     * @param employeeId - id of employee to edit
     * @return
     */
    public boolean editEmploymentFilePay(String employeeId) {
        boolean success = true;    // indicates whether edit was successful or not
        File tempFile = new File("tempPersonalFile.csv");
        String readLine; // string to temporarily store line read from file
        String splitBy = ",";
        String[] record;
        try {
            // Create file reader object
            FileReader reader = new FileReader(employmentDetailsFile);
            BufferedReader buffReader = new BufferedReader(reader);

            FileWriter writer = new FileWriter(tempFile);
            writer.write(buffReader.readLine());    // write headers to temp file

            // Loop through each record of the file until end of file reached
            while (((readLine = buffReader.readLine()) != null)) {
                record = readLine.split(splitBy);
                // if employee id matches current record's id
                if (employeeId.equals(record[0])) {
                    // write the updated record to temp file
                    writer.write("\n" + getUpdatedEmploymentPayRecord());
                } else {
                    // otherwise write the original record
                    writer.write("\n" + readLine);
                }
            }

            // close writer and reader
            reader.close();
            buffReader.close();
            writer.close();
            // delete old file
            if (employmentDetailsFile.delete()) {
                // rename temp file
                if (!tempFile.renameTo(employmentDetailsFile)) {
                    success = false;
                }
            } else {
                success = false;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            success = false;
        } catch (IOException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            success = false;
        }
        return success;
    }

    public boolean deleteEmployee(String employeeId) {
        boolean success = true;
        return success;
    }

    /**
     * searches for existing employee in the personal file
     *
     * @param employeeId employee id to search for
     * @return boolean whether employee was found or not
     */
    public boolean searchLogin(String employeeId) {
        boolean found = false;
        String line;    // string to temporarily store line read from file
        String[] record = null;
        try {
            // Create file reader object
            FileReader loginRead = new FileReader(loginFile);
            BufferedReader loginBuff = new BufferedReader(loginRead);

            // Loop through each record of the file until search item found or end of file reached
            while (((line = loginBuff.readLine()) != null) && (found == false)) {
                record = line.split(",");    // array holds last line read

                // checks if record matches
                if (record[0].equals(employeeId)) {
                    // initialise employment
                    setLoginRecord(line);

                    // set found to true to stop looping
                    found = true;
                }
            }
            loginRead.close();
            loginBuff.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return found;
    }

    /**
     * searches for existing employee in the personal file
     *
     * @param employeeId employee id to search for
     * @return boolean whether employee was found or not
     */
    public boolean searchPersonal(String employeeId) {
        boolean found = false;
        String line;    // string to temporarily store line read from file
        String[] record = null;
        try {
            // Create file reader object
            FileReader personalReader = new FileReader(personalDetailsFile);
            BufferedReader personalBuff = new BufferedReader(personalReader);

            // Loop through each record of the file until search item found or end of file reached
            while (((line = personalBuff.readLine()) != null) && (found == false)) {
                record = line.split(",");    // array holds last line read

                // checks if record matches
                if (record[0].equals(employeeId)) {
                    // initialise employment
                    setPersonalRecord(line);

                    // set found to true to stop looping
                    found = true;
                }
            }
            personalReader.close();
            personalBuff.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return found;
    }

    /**
     * searches for existing employee in the employment file
     *
     * @param employeeId employee id to search for
     * @return boolean whether employee was found or not
     */
    public boolean searchEmployment(String employeeId) {
        boolean found = false;
        String line;    // string to temporarily store line read from file
        String[] record = null;
        try {
            // Create file reader object
            FileReader employmentReader = new FileReader(employmentDetailsFile);
            BufferedReader employmentBuff = new BufferedReader(employmentReader);

            // Loop through each record of the file until search item found or end of file reached
            while (((line = employmentBuff.readLine()) != null) && (found == false)) {
                record = line.split(",");    // array holds last line read

                // checks if record matches
                if (record[0].equals(employeeId)) {
                    // initialise employment
                    setEmploymentRecord(line);
                    setDepartmentDetails(departmentNum);

                    // set found to true to stop looping
                    found = true;
                }
            }
            employmentReader.close();
            employmentBuff.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return found;
    }

    /**
     * searches for an employee in the personal and employment files
     *
     * @param employeeId employee id to search for
     */
    public void searchEmployee(String employeeId) {
        if (searchPersonal(employeeId) == false) {
            isEmployeeFound = false;
        }
        if (searchEmployment(employeeId) == false) {
            isEmployeeFound = false;
        }
        if (isEmployeeFound == false) {
            JOptionPane.showMessageDialog(null, "Employee " + employeeId + " not found", "Employee search", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void updatePay(BigDecimal periodGross, BigDecimal periodTaxable, BigDecimal periodTax, BigDecimal periodNationalContribution, BigDecimal periodEmployerContribution, BigDecimal periodNet) {
        this.lastGrossStr = periodGross.toString();
        this.lastTaxableStr = periodTaxable.toString();
        this.lastIncomeTaxStr = periodTax.toString();
        this.lastNationalContributionStr = periodNationalContribution.toString();
        this.lastEmployerContributionStr = periodEmployerContribution.toString();
        this.lastNetStr = periodNet.toString();
        BigDecimal yearGross = new BigDecimal(yearGrossStr);
        BigDecimal yearTaxable = new BigDecimal(yearTaxableStr);
        BigDecimal yearIncomeTax = new BigDecimal(yearIncomeTaxStr);
        BigDecimal yearNationalContribution = new BigDecimal(yearNationalContributionStr);
        BigDecimal yearEmployerContribution = new BigDecimal(yearEmployerContributionStr);
        BigDecimal yearNet = new BigDecimal(yearNetStr);

        yearGross = yearGross.add(periodGross);
        yearTaxable = yearTaxable.add(periodTaxable);
        yearIncomeTax = yearIncomeTax.add(periodTax);
        yearNationalContribution = yearNationalContribution.add(periodNationalContribution);
        yearEmployerContribution = yearEmployerContribution.add(periodEmployerContribution);
        yearNet = yearNet.add(periodNet);

        yearGrossStr = yearGross.toString();
        yearTaxableStr = yearTaxable.toString();
        yearIncomeTaxStr = yearIncomeTax.toString();
        yearNationalContributionStr = yearNationalContribution.toString();
        yearEmployerContributionStr = yearEmployerContribution.toString();
        yearNetStr = yearNet.toString();
    }

    public void removeEmployee(String removeId) {
        boolean removeSuccess = true;
        if (addToLeavers(removeId)) {
            if (!removeFromFile(loginFileName, removeId)) {
                removeSuccess = false;
            }
            if (!removeFromFile(securityFileName, removeId)) {
                removeSuccess = false;
            }
            if (!removeFromFile(personalDetailsFileName, removeId)) {
                removeSuccess = false;
            }
            if (!removeFromFile(employmentDetailsFileName, removeId)) {
                removeSuccess = false;
            }
        } else {
            removeSuccess = false;
        }

        if (removeSuccess == false) {
            JOptionPane.showMessageDialog(null, "Could not remove employee", "Remove employee", JOptionPane.INFORMATION_MESSAGE);
        } else {

            JOptionPane.showMessageDialog(null, "Employee successfully removed", "Remove employee", JOptionPane.INFORMATION_MESSAGE);
        }

    }

    private boolean removeFromFile(String fileName, String removeId) {
        boolean success = true;
        String originalFileName = employeeFileDir + fileName + ".csv";
        String tempFileName = employeeFileDir + "temp" + fileName + ".csv";
        File originalFile = new File(originalFileName);
        File tempFile = new File(tempFileName);
        String readLine; // string to temporarily store line read from file
        String splitBy = ",";
        String[] record;
        try {
            // Create file reader object
            FileReader reader = new FileReader(originalFile);
            BufferedReader buffReader = new BufferedReader(reader);

            FileWriter writer = new FileWriter(tempFile);
            writer.write(buffReader.readLine());    // write headers to temp file

            // Loop through each record of the file until end of file reached
            while (((readLine = buffReader.readLine()) != null)) {
                record = readLine.split(splitBy);
                // if employee id matches current record's id
                if (removeId.equals(record[0])) {
                    continue;
                }
                // otherwise write the original record
                writer.write("\n" + readLine);
            }

            // close writer and reader
            reader.close();
            buffReader.close();
            writer.close();
//             delete old file
            if (originalFile.delete()) {
                // rename temp file
                if (!tempFile.renameTo(originalFile)) {
                    success = false;
                }
            } else {
                success = false;
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            success = false;
        } catch (IOException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            success = false;
        }
        return success;
    }

    private boolean addToLeavers(String leaverId) {
        boolean addSuccess = true;
        searchEmployee(leaverId);
        try {
            // Create file writer objects
            FileWriter leaverPersonalWriter = new FileWriter(leaverPersonalDetailsFile, true);
            FileWriter leaverEmploymentWriter = new FileWriter(leaverEmploymentDetailsFile, true);
            // write record to files plus new line
            leaverPersonalWriter.write("\n" + getPersonalRecord());
            leaverEmploymentWriter.write("\n" + getEmploymentRecord());
            leaverPersonalWriter.close();
            leaverEmploymentWriter.close();
        } catch (IOException ex) {
            System.out.println("Error adding new employee");
            addSuccess = false;
        }
        return addSuccess;
    }
}
