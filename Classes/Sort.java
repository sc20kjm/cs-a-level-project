package Classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author MAD16833376
 */
public class Sort {

    public void serial() {

    }

    /**
     * Quick sort algorithm
     *
     * @param arr // array to sort
     * @param first // first value in array
     * @param last // last value in array
     * @param column
     * @return
     */
    public int[][] quickAsc(int[][] arr, int first, int last, int column) {
        // moves all values less than pivot to the left and values higher to the right
        int left = first;
        int right = last;
        int pivot = arr[(left + right) / 2][column];
        int temp;

        while (left <= right) {
            while (arr[left][column] < pivot) {
                left++;
            }
            while (arr[right][column] > pivot) {
                right--;
            }
            if (left <= right) {
                // swap values
                temp = arr[left][column];
                arr[left][column] = arr[right][column];
                arr[right][column] = temp;
                // move to next position on both sides
                left++;
                right--;
            }
            if (first < right) {
                quickAsc(arr, first, left - 1, column);
            }
            if (left < last) {
                quickAsc(arr, right + 1, last, column);
            }
        }
        return arr;
    }

    /**
     * Quick sort algorithm
     * @param array - array to sort
     * @param first - first position of array
     * @param last - last position of array
     * @param column - column to sort by
     * @return - returns the sorted array
     */
    public String[][] quickAsc(String[][] array, int first, int last, int column) {
        // moves all values less than pivot to the left and values higher to the right
        int left = first;
        int right = last;
        String pivot = array[(left + right) / 2][column];
        String[] temp;

        while (left <= right) {
            while (array[left][column].compareToIgnoreCase(pivot) < 0) {
                left++;
            }
            while (array[right][column].compareToIgnoreCase(pivot) > 0) {
                right--;
            }
            if (left <= right) {
                // swap values
                temp = array[left];
                array[left] = array[right];
                array[right] = temp;
                // move to next position on both sides
                left++;
                right--;
            }
            if (first < right) {
                array = quickAsc(array, first, left - 1, column);
            }
            if (left < last) {
                array = quickAsc(array, right + 1, last, column);
            }
        }
        return array;
    }
    
    /**
     * Quick sort algorithm
     * @param array - array to sort
     * @param first - first position of array
     * @param last - last position of array
     * @param column - column to sort by
     * @return - returns the sorted array
     */
    public String[][] quickDesc(String[][] array, int first, int last, int column) {
        // moves all values less than pivot to the left and values higher to the right
        int left = first;
        int right = last;
        String pivot = array[(left + right) / 2][column];
        String[] temp;

        while (left < right) {
            while (array[left][column].compareToIgnoreCase(pivot) > 0) {
                left++;
            }
            while (array[right][column].compareToIgnoreCase(pivot) < 0) {
                right--;
            }
            if (left < right) {
                // swap values
                temp = array[left];
                array[left] = array[right];
                array[right] = temp;
                // move to next position on both sides
                left++;
                right--;
            }
            else {
                return array;
            }
            if (first < right) {
                array = quickDesc(array, first, left - 1, column);
            }
            if (left < last) {
                array = quickDesc(array, right + 1, last, column);
            }
        }
        return array;
    }

    /**
     * Quick sort algorithm
     *
     * @param array - array to sort
     * @param first - first position of array
     * @param last - last position of array
     * @param column - column to sort by
     * @return - returns the sorted array
     */
    public String[][] quickRecordDouble(String[][] array, int first, int last, int column) {
//        // moves all values less than pivot to the left and values higher to the right
//        int left = first;
//        int right = last;
//        String pivot = (array[(left + right) / 2][column]);
//        String[] temp;
//
//        while (left <= right) {
//            while (Double.parseDouble(array[left][column]) < Double.parseDouble(pivot)) {
//                left++;
//            }
//            while (Double.parseDouble(array[right][column]) > Double.parseDouble(pivot)) {
//                right--;
//            }
//            if (left <= right) {
//                // swap values
//                temp = array[left];
//                array[left] = array[right];
//                array[right] = temp;
//                // move to next position on both sides
//                left++;
//                right--;
//            }
//            if (first < right) {
//                array = quick(array, first, left - 1, column);
//            }
//            if (left < last) {
//                array = quick(array, right + 1, last, column);
//            }
//        }
        return array;
    }

    /**
     * Quick sort algorithm
     *
     * @param array - array to sort
     * @param first - first position of array
     * @param last - last position of array
     * @param column - column to sort by
     * @return - returns the sorted array
     */
    public String[][] quickRecordInt(String[][] array, int first, int last, int column) {
//        // moves all values less than pivot to the left and values higher to the right
//        int left = first;
//        int right = last;
//        int pivot = Integer.parseInt(array[(left + right) / 2][column].replace(".", ""));
//        String[] temp;
//
//        while (left <= right) {
//            while (Integer.parseInt(array[left][column].replace(".", "")) < pivot) {
//                left++;
//            }
//            while (Integer.parseInt(array[right][column].replace(".", "")) > pivot) {
//                right--;
//            }
//            if (left <= right) {
//                // swap values
//                temp = array[left];
//                array[left] = array[right];
//                array[right] = temp;
//                // move to next position on both sides
//                left++;
//                right--;
//            }
//            if (first < right) {
//                array = quick(array, first, left - 1, column);
//            }
//            if (left < last) {
//                array = quick(array, right + 1, last, column);
//            }
//        }
        return array;
    }

    public String[][] quickDateAsc(String[][] array, int first, int last, int column) throws ParseException {
        // moves all values less than pivot to the left and values higher to the right
        int left = first;
        int right = last;
        String pivotLocation = array[(left + right) / 2][column];
        Date pivotDate = new SimpleDateFormat("dd/MM/yyyy").parse(pivotLocation);
        Date leftDate;
        Date rightDate;
        String[] temp;

        while (left <= right) {
            leftDate = new SimpleDateFormat("dd/MM/yyyy").parse(array[left][column]);
            rightDate = new SimpleDateFormat("dd/MM/yyyy").parse(array[right][column]);
            while (leftDate.compareTo(pivotDate) < 0) {
                left++;
                leftDate = new SimpleDateFormat("dd/MM/yyyy").parse(array[left][column]);
            }
            while (rightDate.compareTo(pivotDate) > 0) {
                right--;
                rightDate = new SimpleDateFormat("dd/MM/yyyy").parse(array[right][column]);
            }
            if (left <= right) {
                // swap values
                temp = array[left];
                array[left] = array[right];
                array[right] = temp;
                // move to next position on both sides
                left++;
                right--;
            }
            if (first < right) {
                array = quickDateAsc(array, first, left - 1, column);
            }
            if (left < last) {
                array = quickDateAsc(array, right + 1, last, column);
            }
        }
        return array;
    }

    /**
     * Quick sort algorithm
     *
     * @param array // array to sort
     * @param first // first value in array
     * @param last // last value in array
     * @return
     */
    public int[] quickAsc(int[] array, int first, int last) {
        // moves all values less than pivot to the left and values higher to the right
        int left = first;
        int right = last;
        int pivot = array[(left + right) / 2];
        int temp;

        while (left <= right) {
            while (array[left] < pivot) {
                left++;
            }
            while (array[right] > pivot) {
                right--;
            }
            if (left <= right) {
                // swap values
                temp = array[left];
                array[left] = array[right];
                array[right] = temp;
                // move to next position on both sides
                left++;
                right--;
            }
            if (first < right) {
                array = quickAsc(array, first, left - 1);
            }
            if (left < last) {
                array = quickAsc(array, right + 1, last);
            }
        }
        return array;
    }

    public String[] quickAsc(String[] array, int first, int last) {
        // moves all values less than pivot to the left and values higher to the right
        int left = first;
        int right = last;
        String pivot = array[(left + right) / 2];
        String temp;

        while (left <= right) {
            while (array[left].compareToIgnoreCase(pivot) < 0) {
                left++;
            }
            while (array[right].compareToIgnoreCase(pivot) > 0) {
                right--;
            }
            if (left <= right) {
                // swap values
                temp = array[left];
                array[left] = array[right];
                array[right] = temp;
                // move to next position on both sides
                left++;
                right--;
            }
            if (first < right) {
                array = quickAsc(array, first, left - 1);
            }
            if (left < last) {
                array = quickAsc(array, right + 1, last);
            }
        }
        return array;
    }

    /**
     * Bubble sort algorithm
     *
     * @param array - array to sort
     * @param first - first position of array
     * @param last - last position of array
     * @param column - column to sort by
     * @return - returns the sorted array
     */
    public String[][] bubbleRecordIntAsc(String[][] array, int first, int last, int column) {
        // moves all values less than pivot to the left and values higher to the right
        int i;
        int j;
        String[] temp;
        for (i = 0; i < array.length; i++) {
            for (j = 1; j < (array.length - i); j++) {
                if (Double.parseDouble(array[j - 1][column]) > Double.parseDouble(array[j][column])) {
                    //swap elements  
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }
    
    /**
     * Bubble sort algorithm
     *
     * @param array - array to sort
     * @param first - first position of array
     * @param last - last position of array
     * @param column - column to sort by
     * @return - returns the sorted array
     */
    public String[][] bubbleRecordIntDesc(String[][] array, int first, int last, int column) {
        // moves all values less than pivot to the left and values higher to the right
        int i;
        int j;
        String[] temp;
        for (i = 0; i < array.length; i++) {
            for (j = 1; j < (array.length - i); j++) {
                if (Double.parseDouble(array[j - 1][column]) < Double.parseDouble(array[j][column])) {
                    //swap elements  
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }

}
