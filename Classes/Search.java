package Classes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author MAD16833376
 */
public class Search {
    final String employeeFileDir = "Employee Files\\"; // employee files location
    final String systemFileDir = "System Files\\";   // system files location
    final String leaversDir = employeeFileDir + "Leavers\\";

    final String loginFileName = "Login Details";   // login file name
    final String securityFileName = "Security Details"; // security file name
    final String personalDetailsFileName = "Personal Details";  // personal details file name
    final String employmentDetailsFileName = "Employment Details";  // employment details file name
    final String departmentDetailsFileName = "Department Details";  // department details file name

    final File loginFile = new File(employeeFileDir + loginFileName + ".csv");  // login file
    final File securityFile = new File(employeeFileDir + securityFileName + ".csv");        // security file
    final File personalDetailsFile = new File(employeeFileDir + personalDetailsFileName + ".csv");  // personal details file
    final File employmentDetailsFile = new File(employeeFileDir + employmentDetailsFileName + ".csv");  // employment details file
    final File departmentDetailsFile = new File(employeeFileDir + departmentDetailsFileName + ".csv");  // department details file
    final File leaverPersonalDetailsFile = new File(leaversDir + personalDetailsFileName + ".csv");  // personal details file
    final File leaverEmploymentDetailsFile = new File(leaversDir + employmentDetailsFileName + ".csv");  // employment details file
    
    private ArrayList<String> searchMultiple = new ArrayList<>();

    /**
     * searches file for matching query
     *
     * @param fileName file to search from
     * @param query item the user is searching for
     * @param pointer data is stored in records so this identifies where in the
     * record each query is stored
     * @return returns the whole record if a match is found
     */
    public static ArrayList<String> recordPtrSerial(File fileName, String query, int pointer) {
        // searches file serial for matching query
        boolean found = false;   // flag to indicate if search item has been found
        String line;    // string to temporarily store line read from file
        String[] record = null;
        ArrayList<String> matchingRecords = new ArrayList<String>();
        try {
            // Create file reader object
            FileReader reader = new FileReader(fileName);
            BufferedReader buffReader = new BufferedReader(reader);

            // Loop through each record of the file until search item found or end of file reached
            while (((line = buffReader.readLine()) != null) && (found == false)) {
                record = line.split(",");    // array holds last line read

                // checks if record matches
                if (record[pointer].toLowerCase().equals(query.toLowerCase())) {
                    // add record to array list
                    matchingRecords.add(line);
                }
            }
            if (found == false) {
                record = null;
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return matchingRecords;
    }

    /**
     * Returns the record from the specified file of the provided employee ID
     * @param file  // file to search
     * @param employeeId    // employee to search for
     * @return returns the record of the employee from the specified file
     */
    public String[] recordIdSerial(File file, String employeeId) {
        // searches file serial for matching query
        boolean found = false;   // flag to indicate if search item has been found
        String line;    // string to temporarily store line read from file
        String[] record = null;
        try {
            // Create file reader object
            FileReader reader = new FileReader(file);
            BufferedReader buffReader = new BufferedReader(reader);

            // Loop through each record of the file until search item found or end of file reached
            while (((line = buffReader.readLine()) != null) && (found == false)) {
                record = line.split(",");    // array holds last line read

                // checks if record matches
                if (record[0].equals(employeeId)) {
                    // set found to true to stop looping
                    found = true;
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return record;
    }

    /**
     * Binary search returning a record if the search query exists inside it
     *
     * @param fileName Name of file
     * @param query Item to search for
     * @param index
     * @return
     * @throws Exception
     */
    public String[] recordBinary(File fileName, String query, int index) throws Exception {
        // Binary search
//        String line;
//        String splitBy = ",";
        String[] record = null;
//        String[] queryArray;
//        ArrayList<String> queryArrayList = new ArrayList();
//        boolean found = false;
//        int first = 0;
//        int last;
//        int mid;
//        
//        try{
//            FileReader read = new FileReader(fileName);
//            BufferedReader buffRead = new BufferedReader(read);
//            buffRead.readLine();
//            
//            while((line = buffRead.readLine()) != null){
//                record = line.split(splitBy);
//                queryArrayList.add(record[index]);
//            }
//            queryArray = queryArrayList.toArray(new String[queryArrayList.size()]);
//            last = queryArray.length - 1;
//            do{
//                mid = (first + last) / 2;
//                if query
//            }while(found == false || last > first);
//        }
//        catch(IOException e){
//            
//        }

        /* pseudocode:
        
        first = 1
        last = n
        found = false

        repeat
        
            mid = (first + last) Div 2
            if searchValue = values[mid] then
		found = true
            endif
            if searchValue < values[mid] then
		last = mid – 1
            else
		first = mid + 1
        	endif
        until found or (last < first)
         */
        return record;

    }

    /**
     * Searches file for the value of a specific item
     *
     * @param fileName name of file to search in
     * @param itemName name of item to look for
     * @param itemNamePtr position of the name
     * @param itemPtr position of the actual item
     * @return returns the value of the item searched for
     */
    public String itemString(File fileName, String itemName, int itemNamePtr, int itemPtr) {
        String itemValue = "";
        String line;
        String[] record;
        try {
            // Create file reader object
            FileReader reader = new FileReader(fileName);
            BufferedReader buffReader = new BufferedReader(reader);

            // Loop through each record of the file until search item found or end of file reached
            while ((line = buffReader.readLine()) != null) {
                record = line.split(",");    // array holds last line read
                if (record[itemNamePtr].equals(itemName)) {
                    if (record[itemPtr] == null) {
                        itemValue = "";
                    } else {
                        itemValue = record[itemPtr];
                    }
                }
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return itemValue;
    }

//    public boolean existingBinary(File fileName, String query, int index) throws Exception{
//        // Binary search
//        String
//    }
    
    /**
     * searches database for matching query
     *
     * @param query item the user is searching for
     * @param pointer data is stored in records so this identifies where in the
     * record each query is stored
     * @return returns the whole record if a match is found
     */
    public boolean personalMultiple(String query, int pointer) {
        // searches file serial for matching query
        boolean found = false;   // flag to indicate if search item has been found
        String line;    // string to temporarily store line read from file
        String[] record;
        try {
            // Create file reader object
            FileReader reader = new FileReader(personalDetailsFile);
            BufferedReader buffReader = new BufferedReader(reader);
            buffReader.readLine();  // skips headers

            // Loop through each record of the file until search item found or end of file reached
            while (((line = buffReader.readLine()) != null)) {
                record = line.split(",");    // array holds last line read

                // checks if record matches
                if (record[pointer].toLowerCase().startsWith(query.toLowerCase())) {
                    // add id to array list
                    searchMultiple.add(record[0]);
                    found = true;
                }
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return found;
    }
    
    /**
     * searches database for matching query
     *
     * @param query item the user is searching for
     * @param pointer data is stored in records so this identifies where in the
     * record each query is stored
     * @return returns the whole record if a match is found
     */
    public boolean employmentMultiple(String query, int pointer) {
        // searches file serial for matching query
        boolean found = false;   // flag to indicate if search item has been found
        String line;    // string to temporarily store line read from file
        String[] record;
        try {
            // Create file reader object
            FileReader reader = new FileReader(employmentDetailsFile);
            BufferedReader buffReader = new BufferedReader(reader);
            buffReader.readLine();  // skips headers

            // Loop through each record of the file until search item found or end of file reached
            while (((line = buffReader.readLine()) != null)) {
                record = line.split(",");    // array holds last line read

                // checks if record matches
                if (record[pointer].toLowerCase().startsWith(query.toLowerCase())) {
                    // add id to array list
                    searchMultiple.add(record[0]);
                    found = true;
                }
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return found;
    }
    
    /**
     * searches file for matching query
     *
     * @param query item the user is searching for
     * @param pointer data is stored in records so this identifies where in the
     * record each query is stored
     * @return returns the whole record if a match is found
     */
    public boolean filterPersonal(String query, int pointer) {
        // searches file serial for matching query
        boolean found = false;   // flag to indicate if search item has been found
        String line;    // string to temporarily store line read from file
        String[] record;
        try {
            // Create file reader object
            FileReader reader = new FileReader(personalDetailsFile);
            BufferedReader buffReader = new BufferedReader(reader);
            buffReader.readLine();  // skips headers

            // Loop through each record of the file until search item found or end of file reached
            while (((line = buffReader.readLine()) != null)) {
                record = line.split(",");    // array holds last line read

                // checks if record matches
                if (record[pointer].toLowerCase().startsWith(query.toLowerCase())) {
                    // add id to array list
                    searchMultiple.add(record[0]);
                    found = true;
                }
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return found;
    }
    
    /**
     * searches file for matching query
     *
     * @param query item the user is searching for
     * @param pointer data is stored in records so this identifies where in the
     * record each query is stored
     * @return returns the whole record if a match is found
     */
    public boolean filterEmployment(String query, int pointer) {
        // searches file serial for matching query
        boolean found = false;   // flag to indicate if search item has been found
        String line;    // string to temporarily store line read from file
        String[] record;
        try {
            // Create file reader object
            FileReader reader = new FileReader(employmentDetailsFile);
            BufferedReader buffReader = new BufferedReader(reader);
            buffReader.readLine();  // skips headers

            // Loop through each record of the file until search item found or end of file reached
            while (((line = buffReader.readLine()) != null)) {
                record = line.split(",");    // array holds last line read

                // checks if record matches
                if (record[pointer].toLowerCase().startsWith(query.toLowerCase())) {
                    // add id to array list
                    searchMultiple.add(record[0]);
                    found = true;
                }
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return found;
    }
    
    public ArrayList<String> getResultsMultiple(){
        return this.searchMultiple;
    }
}
